
--todo: average cast damage isn't sorting correctly
--todo: uptime has a weird sort
--todo: hover over column headers highlight and click motion (button going down and up on click)
--todo: targets of the spell tooltip in the left and more information about the spell above the line
--todo: rename damage done to healing done when showing healing
--todo: in healing set the spec icon of the target player
--todo: targets need 1 more line and a header text telling it's healing done

do
	local Details = Details
	if (not Details) then
		print ("Details! Not Found.")
		return
	end
	
	local _
	local DF = _G.DetailsFramework
	local _ipairs = ipairs
	local unpack = _G.unpack

	--> minimal details version required to run this plugin
	local MINIMAL_DETAILS_VERSION_REQUIRED = 136
	local ABREAKDOWN_VERSION = "v1.0.0"
	
	--> create a plugin object
	local aBreakdown = Details:NewPluginObject ("Details_AdvancedPlayerBreakdown", _G.DETAILSPLUGIN_ALWAYSENABLED)
	--> just localizing here the plugin's main frame
	local frame = aBreakdown.Frame
	--> set the description
	aBreakdown:SetPluginDescription ("Enhance the player breakdown window.")
	
	--> when receiving an event from details, handle it here
	local handle_details_event = function (event, ...)
	
		if (event == "COMBAT_PLAYER_ENTER") then
			

		elseif (event == "COMBAT_PLAYER_LEAVE") then
			
			
		elseif (event == "PLUGIN_DISABLED") then
			--> plugin has been disabled at the details options panel
		
		elseif (event == "PLUGIN_ENABLED") then
			--> plugin has been enabled at the details options panel
		
		elseif (event == "DETAILS_DATA_SEGMENTREMOVED") then
			--> old segment got deleted by the segment limit
		
		elseif (event == "DETAILS_DATA_RESET") then
			--> combat data got wiped

		end
		
	end
	
	function aBreakdown.InstallUtilityTab()
		local tabName = "PlayerUtility"
		local tabNameLoc = "Utility"
		local tabIcon = {
			texture = [[Interface\WORLDSTATEFRAME\NeutralTower]],
			coords = {3/32, 21/32, 0, 20/32},
			width = 16,
			height = 16,
		}
		
		local canShowTab = function (tabOBject, playerObject)
			local combat = Details:GetCombatFromBreakdownWindow()
			if (combat) then
				local miscPlayerObject = combat:GetActor (4, playerObject.nome)
				if (miscPlayerObject) then
					tabOBject.frame:Show()
					return true
				end
			end
			return false
		end
		
		local fillTab = function (tab, playerObject, combat)
			
			local playerName = playerObject:GetName()
			local energyObject = combat:GetActor (3, playerName)
			local miscObject = combat:GetActor (4, playerName)
			
			--cooldowns
			local cooldownsTable = {
				playerName = playerName,
				combatObject = combat
			}
			
			--interrupts
			local interruptsTable = {
				playerName = playerName,
				combatObject = combat
			}
			
			--dispells
			local dispellsTable = {
				playerName = playerName,
				combatObject = combat
			}
			
			--resources
			local resourcesTable = {
				playerName = playerName,
				combatObject = combat
			}
			
			--deaths
			local deathsTable = {
				playerName = playerName,
				combatObject = combat
			}
			
			--crowd control
			local ccTable = {
				playerName = playerName,
				combatObject = combat,
				playerObject = miscObject
			}
			
			if (miscObject) then
				--cooldowns
				local cooldownSpells = miscObject.cooldowns_defensive_spells
				if (cooldownSpells) then
					for spellId, spellTable in pairs (cooldownSpells._ActorTable) do
						tinsert (cooldownsTable, {spellId, spellTable.counter})
					end
					table.sort (cooldownsTable, DetailsFramework.SortOrder2)
				end
				
				--interrupts
				local interruptSpells = miscObject.interrupt_spells
				if (interruptSpells) then
					for spellId, spellTable in pairs (interruptSpells._ActorTable) do
						for spellId, amount in pairs (spellTable.interrompeu_oque) do
							tinsert (interruptsTable, {spellId, amount})
						end
					end
					table.sort (interruptsTable, DetailsFramework.SortOrder2)
				end
				
				--dispells
				local dispellSpells = miscObject.dispell_spells
				if (dispellSpells) then
					for spellId, dispellSpell in pairs (dispellSpells._ActorTable) do
						for spellId, amount in pairs (dispellSpell.dispell_oque) do
							tinsert (dispellsTable, {spellId, amount})
						end
					end
				end
				
				--resources
				if (energyObject) then
					local totalGenerated = 0
					for spellid, spellObject in pairs (energyObject.spells._ActorTable) do
						tinsert (resourcesTable, {spellObject, spellObject.total, spellObject.totalover})
						totalGenerated = totalGenerated + spellObject.total
					end
					
					table.sort (resourcesTable, _detalhes.Sort2)
					resourcesTable.totalGenerated = totalGenerated
					resourcesTable.total = energyObject.total
					resourcesTable.passiveOver = energyObject.passiveover
				end
				
				--deaths
				local combatDeaths = combat.last_events_tables
				for i = 1, #combatDeaths do
					local deathTable = combatDeaths [i]
					
					--check if this death is for the player
					if (deathTable [3] == playerName) then
						tinsert (deathsTable, deathTable)
					end
				end
				
				--crowd control
				--amount of crowd control made by this player
				local ccDone = floor (miscObject.cc_done or 0)
				if (miscObject.cc_done_targets) then
					for targetName, amountCC in pairs (miscObject.cc_done_targets) do
						tinsert (ccTable, {targetName, amountCC})
					end
					table.sort (ccTable, _detalhes.Sort2)
				end
				
				--[=[
				if (miscObject.cc_done_spells) then
					for spellid, spell in pairs (miscObject.cc_done_spells._ActorTable) do
						tinsert (ccTable, {spellid, spell.counter})
					end

					table.sort (spells, _detalhes.Sort2)
				end
				--]=]

			end
			
			--cooldowns
			tab.frame.cooldownsFrame:SetData (cooldownsTable)
			tab.frame.cooldownsFrame:Refresh()
			
			--interrupts
			tab.frame.interruptsFrame:SetData (interruptsTable)
			tab.frame.interruptsFrame:Refresh()
			
			--dispells
			tab.frame.dispellsFrame:SetData (dispellsTable)
			tab.frame.dispellsFrame:Refresh()
			
			--resources
			tab.frame.resourcesFrame:SetData (resourcesTable)
			tab.frame.resourcesFrame:Refresh()
			
			--deaths
			tab.frame.deathsFrame:SetData (deathsTable)
			tab.frame.deathsFrame:Refresh()
			
			--crowd control
			tab.frame.crowdControlFrame:SetData (ccTable)
			tab.frame.crowdControlFrame:Refresh()
			
		end
		
		local onClickTab = function ()
			tab.frame:Show()
		end
		
		local onCreatedTab = function (tab, frame)
			
			local yStart = -20
			local scrollWidth = 235
			local scrollHeight = 165
			local scrollLines = 8
			local lineHeight = 19
			local lineBackdrop = {bgFile = [[Interface\Tooltips\UI-Tooltip-Background]], tileSize = 64, tile = true}
			local lineBackdropColor = {.2, .2, .2, 0.2}
			local lineBackdropColorOnEnter = {.2, .2, .2, 0.6}
			local scrollBackdrop = {edgeFile = [[Interface\Buttons\WHITE8X8]], edgeSize = 1, bgFile = [[Interface\Tooltips\UI-Tooltip-Background]], tileSize = 64, tile = true}
			local scrollBackdropBorderColor = {0, 0, 0, 1}
			
			local iconBorderShape = {.1, .9, .1, .9}
			local iconSize = 16
			
			local add_cooltip_backdrop = function()
				GameCooltip2:AddStatusBar (100, 1, .2, .2, .2, .4, false, false, "Details Flat")
			end
			local add_player_icon = function (playerName)
				local textureInfo = Details.UnitTexture (playerName)
				if (textureInfo) then
					GameCooltip2:AddIcon (textureInfo.specTexture, 1, 1, iconSize, iconSize, textureInfo.specLeft, textureInfo.specRight, textureInfo.specTop, textureInfo.specBottom)
				end
			end
			
			local onLeaveFunc = function (self)
				GameCooltip2:Hide()
				self:SetBackdropColor (unpack (lineBackdropColor))
			end
			
			--scroll template to use on all widgets
			local createScrollFrame = function (parent, name, refreshFunc, onEnterFunc, amoutLines, scrollTitle)
				local scrollFrame = DetailsFramework:CreateScrollBox (parent, "$parent" .. name, refreshFunc, {}, scrollWidth, scrollHeight, scrollLines, lineHeight)
				DF:ReskinSlider (scrollFrame)
				scrollFrame:SetBackdrop (scrollBackdrop)
				scrollFrame:SetBackdropColor (unpack (lineBackdropColor))
				scrollFrame:SetBackdropBorderColor (unpack (scrollBackdropBorderColor))
				
				local scrollTitleLabel = DetailsFramework:CreateLabel (scrollFrame, scrollTitle)
				scrollTitleLabel:SetPoint ("bottomleft", scrollFrame, "topleft", 0, 1)
				
				--create lines
				for i = 1, scrollLines do 
					scrollFrame:CreateLine (function (self, index)
						local line = CreateFrame ("statusbar", "$parentLine" .. index, self, "BackdropTemplate")
						line:SetPoint ("topleft", self, "topleft", 1, -((index-1) * (lineHeight + 1)) - 1)
						line:SetSize (scrollWidth - 2, lineHeight)
						line:SetScript ("OnEnter", function (self) line:SetBackdropColor (unpack (lineBackdropColorOnEnter)); local _ = onEnterFunc and onEnterFunc (self) end)
						line:SetScript ("OnLeave", onLeaveFunc)
						
						line:SetBackdrop (lineBackdrop)
						line:SetBackdropColor (unpack (lineBackdropColor))
						
						line.spellIcon = DetailsFramework:CreateImage (line)
						line.spellIcon:SetSize (lineHeight - 2, lineHeight - 2)
						line.spellIcon:SetPoint ("left", 2, 0)
						
						line.leftText = DetailsFramework:CreateLabel (line)
						line.rightText = DetailsFramework:CreateLabel (line)
						
						line.leftText:SetPoint ("left", line.spellIcon, "right", 2, 0)
						line.rightText:SetPoint ("right", line, "right", -2, 0)
						
						return line
					end)
				end
				
				return scrollFrame
			end

			--cooldowns
			local cooldownRefreshFunc = function (self, data, offset, totalLines)
				local topValue = 0
				for _, t in ipairs (data) do
					if (topValue < t[2]) then
						topValue = t[2]
					end
				end
			
				for i = 1, totalLines do
					local index = i + offset
					local cooldownTable = data [index]
					
					if (cooldownTable) then
						local line = self:GetLine (i)
						
						local spellId = cooldownTable [1]
						local spellName, _, spellIcon = GetSpellInfo (spellId)
						
						line.spellIcon.texture = spellIcon
						line.spellIcon.texcoord = {.1, .9, .1, .9}
						
						line.leftText.text = spellName
						line.rightText.text = cooldownTable [2]
						
						line:SetValue (floor (cooldownTable [2] / topValue * 100))
						
						line.spellId = spellId
						line.playerName = data.playerName
						line.combatObject = data.combatObject
					end
				end
			end
			
			local cooldownOnEnterFunc = function (self)
				local playerName = self.playerName
				local combatObject = self.combatObject
				
				local miscObject = combatObject:GetActor (4, playerName)
				if (miscObject and miscObject.cooldowns_defensive_spells) then
					local cooldownSpell = miscObject.cooldowns_defensive_spells._ActorTable [self.spellId]
					local allTargets = {}
					for targetName, amount in pairs (cooldownSpell.targets) do
						if (amount > 0) then
							tinsert (allTargets, {targetName, amount})
						end
					end
					
					if (#allTargets > 0) then
						table.sort (allTargets, DetailsFramework.SortOrder2)
						GameCooltip2:Preset (2)
						GameCooltip2:SetOwner (self)
						for _, targetTable in ipairs (allTargets) do
							GameCooltip2:AddLine (targetTable[1], targetTable[2])
							add_player_icon (targetTable [1])
							add_cooltip_backdrop()
						end
						GameCooltip2:Show()
					end
				end
			end
			
			local cooldownsFrame = createScrollFrame (frame, "CooldownsScroll", cooldownRefreshFunc, cooldownOnEnterFunc, amoutLines, "Cooldowns")
			frame.cooldownsFrame = cooldownsFrame
			frame.cooldownsFrame:SetPoint ("topleft", frame, "topleft", 0, yStart)
			
			--interrupts
			local interruptRefreshFunc = function (self, data, offset, totalLines)
				local topValue = 0
				for _, t in ipairs (data) do
					if (topValue < t[2]) then
						topValue = t[2]
					end
				end
				
				for i = 1, totalLines do
					local index = i + offset
					local interruptTable = data [index]
					
					if (interruptTable) then
						local line = self:GetLine (i)
						
						local spellId = interruptTable [1]
						local spellName, _, spellIcon = GetSpellInfo (spellId)
						
						line.spellIcon.texture = spellIcon
						line.spellIcon.texcoord = {.1, .9, .1, .9}
						
						line.leftText.text = spellName
						line.rightText.text = interruptTable [2]
						
						line:SetValue (floor (interruptTable [2] / topValue * 100))
						
						line.spellId = spellId
						line.playerName = data.playerName
						line.combatObject = data.combatObject
					end
				end
			end
			
			local interruptOnEnterFunc = function (self)
				--
			end
			
			local interruptsFrame = createScrollFrame (frame, "InterruptsScroll", interruptRefreshFunc, interruptOnEnterFunc, amoutLines, "Interrupts")
			frame.interruptsFrame = interruptsFrame
			frame.interruptsFrame:SetPoint ("topleft", frame.cooldownsFrame, "bottomleft", 0, -20)
			
			
			--dispells
			local dispellRefreshFunc = function (self, data, offset, totalLines)
				local topValue = 0
				for _, t in ipairs (data) do
					if (topValue < t[2]) then
						topValue = t[2]
					end
				end
				
				for i = 1, totalLines do
					local index = i + offset
					local dispellTable = data [index]
					
					if (dispellTable) then
						local line = self:GetLine (i)
						
						local spellId = dispellTable [1]
						local spellName, _, spellIcon = GetSpellInfo (spellId)
						
						line.spellIcon.texture = spellIcon
						line.spellIcon.texcoord = {.1, .9, .1, .9}
						
						line.leftText.text = spellName
						line.rightText.text = dispellTable [2]
						
						line:SetValue (floor (dispellTable [2] / topValue * 100))
						
						line.spellId = spellId
						line.playerName = data.playerName
						line.combatObject = data.combatObject
					end
				end
			end
			
			local dispellOnEnterFunc = function (self)
				local playerName = self.playerName
				local combatObject = self.combatObject
				
				local miscObject = combatObject:GetActor (4, playerName)
				if (miscObject and miscObject.dispell_spells) then
					--who this actor dispelled
					local dispellSpells = {}
					local canShowTooltip = false
					for spellid, spellTable in pairs (miscObject.dispell_spells._ActorTable) do
						local dispellAmount = spellTable.dispell_oque [self.spellId]
						if (dispellAmount) then
							tinsert (dispellSpells, {spellid, dispellAmount})
						end
					end

					if (#dispellSpells > 0) then
						canShowTooltip = true
						table.sort (dispellSpells, DetailsFramework.SortOrder2)
						GameCooltip2:Preset (2)
						GameCooltip2:SetOwner (self)
						for _, dispellTable in ipairs (dispellSpells) do
							local spellName, _, spellIcon = GetSpellInfo (dispellTable [1])
							local amount = dispellTable [2]
							GameCooltip2:AddLine (spellName, amount)
							GameCooltip2:AddIcon (spellIcon, 1, 1, iconSize, iconSize, unpack (iconBorderShape))
							add_cooltip_backdrop()
						end
						
					end

					if (canShowTooltip) then
						GameCooltip2:Show()
					end
				end
			end
			
			local dispellsFrame = createScrollFrame (frame, "DispellsScroll", dispellRefreshFunc, dispellOnEnterFunc, amoutLines, "Dispells")
			frame.dispellsFrame = dispellsFrame
			frame.dispellsFrame:SetPoint ("topleft", frame.cooldownsFrame, "topright", 26, 0)
			
			--resources
			local resourceRefreshFunc = function (self, data, offset, totalLines)
				local topValue = 0
				for _, t in ipairs (data) do
					if (topValue < t[2]) then
						topValue = t[2]
					end
				end

				for i = 1, totalLines do
					local index = i + offset
					local resourceTable = data [index]
					
					if (resourceTable) then
						local line = self:GetLine (i)
						
						local spellObject = resourceTable [1]
						local spellId = spellObject.id
						local spellName, _, spellIcon = GetSpellInfo (spellId)
						local total = resourceTable [2]
						local totalover = resourceTable [3]
						
						line.spellIcon.texture = spellIcon
						line.spellIcon.texcoord = {.1, .9, .1, .9}

						line.leftText.text = spellName
						DetailsFramework:TruncateText (line.leftText, 125)
						
						line.rightText.text = floor (total) .. " (|cFFFF3333" .. floor (totalover or 0) .. "|r | " .. floor (total / data.totalGenerated * 100) .. "%)"
						
						line:SetValue (floor (total / topValue * 100))
						
						line.spellId = spellId
						line.playerName = data.playerName
						line.combatObject = data.combatObject
						
						line.isResourceGenerator = true
						line.isPassiveOverflow = nil
					end
				end

				if (data.passiveOver and data.passiveOver >= 1) then
					local line = self:GetLine (#data + 2)
					line.leftText.text = "Regen Overflow"
					line.rightText.text = "|cFFFF3333" .. floor (data.passiveOver) .. "|r"
					line.spellIcon.texture = [[Interface\COMMON\Indicator-Red]]
					line.spellIcon.texcoord = {.1, .9, .1, .9}
					
					line.isResourceGenerator = nil
					line.isPassiveOverflow = true
				end
			end
			
			local resourceOnEnterFunc = function (self)
				GameCooltip2:Preset (2)
				GameCooltip2:SetOwner (self)
				
				if (self.isResourceGenerator) then
					GameCooltip2:AddLine ("Energy generated by this ability. The value in red is wasted energy (generated after the character is already with full energy)")
					
				elseif (self.isPassiveOverflow) then
					GameCooltip2:AddLine ("Amount of energy wasted on auto regen because the character was sitting on full energy.")
				end
				
				GameCooltip2:Show()
			end
			
			local resourcesFrame = createScrollFrame (frame, "ResourcesScroll", resourceRefreshFunc, resourceOnEnterFunc, amoutLines, "Resources")
			frame.resourcesFrame = resourcesFrame
			frame.resourcesFrame:SetPoint ("topleft", frame.interruptsFrame, "topright", 26, 0)
			
			
			--deaths
			local deathsRefreshFunc = function (self, data, offset, totalLines)
				for i = 1, totalLines do
					local index = i + offset
					local deathTable = data [index]
					
					if (deathTable) then
						local line = self:GetLine (i)
						
						line.spellIcon.texture = [[Interface\WORLDSTATEFRAME\SkullBones]]
						line.spellIcon.texcoord = {0, .5, 0, .5}

						line.leftText.text = "Death #" .. i
						line.rightText.text = deathTable [6]
						
						line:SetValue (100)
						
						line.playerName = data.playerName
						line.combatObject = data.combatObject
						line.deathTable = deathTable
					end
				end
			end
			
			local deathsOnEnterFunc = function (self)
				if (self.deathTable) then
					local instance = Details:GetActiveWindowFromBreakdownWindow()
					_detalhes:ToolTipDead (instance, self.deathTable, self)
				end
			end
			
			local deathsFrame = createScrollFrame (frame, "DeathsScroll", deathsRefreshFunc, deathsOnEnterFunc, amoutLines, "Deaths")
			frame.deathsFrame = deathsFrame
			frame.deathsFrame:SetPoint ("topleft", frame.dispellsFrame, "topright", 26, 0)
			
			
			--crowd control done
			local crowdControlRefreshFunc = function (self, data, offset, totalLines)
				for i = 1, totalLines do
					local index = i + offset
					local ccTargetsTable = data [index]
					
					if (ccTargetsTable) then
						local line = self:GetLine (i)
						
						line.leftText.text = ccTargetsTable [1]
						line.rightText.text = ccTargetsTable [2]
						line:SetValue (100)
						
						line.targetName = ccTargetsTable [1]
						line.playerName = data.playerName
						line.combatObject = data.combatObject
						line.playerObject = data.playerObject
					end
				end
			end
			
			local crowdControlOnEnterFunc = function (self)
				local miscObject = self.playerObject
				local targetName = self.targetName
				
				GameCooltip2:Preset (2)
				GameCooltip2:SetOwner (self)
				local canShowTooltip = false
				
				local spells = miscObject.cc_done_spells._ActorTable
				for spellId, spellTable in pairs (spells) do
					local onThisTarget = spellTable.targets [targetName]
					if (onThisTarget) then
						local spellName, _, spellIcon = GetSpellInfo (spellId)
						local amount = onThisTarget
						GameCooltip2:AddLine (spellName, amount)
						GameCooltip2:AddIcon (spellIcon, 1, 1, iconSize, iconSize, unpack (iconBorderShape))
						add_cooltip_backdrop()
						canShowTooltip = true
					end
				end
				
				if (canShowTooltip) then
					GameCooltip2:Show()
				end
			end
			
			local crowdControlFrame = createScrollFrame (frame, "CrowdControlScroll", crowdControlRefreshFunc, crowdControlOnEnterFunc, amoutLines, "Crowd Control")
			frame.crowdControlFrame = crowdControlFrame
			frame.crowdControlFrame:SetPoint ("topleft", frame.resourcesFrame, "topright", 26, 0)

		end
		
		Details:CreatePlayerDetailsTab (tabName, tabNameLoc, canShowTab, fillTab, nil, onCreatedTab, tabIcon)
	end	
	
	function aBreakdown.InstallAdvancedCompareWindow()
	
		local red = "FFFFAAAA"
		local green = "FFAAFFAA"
		local plus = red .. "-" 
		local minor = green .. "+"
	
		local comparisonFrameSettings = {
			--main player scroll frame
			mainScrollWidth = 250,
			petColor = "|cFFCCBBBB",
			
			--spell scroll
			spellScrollHeight = 300,
			spellLineAmount = 14,
			spellLineHeight = 20,
			
			--target scroll
			targetScrollHeight = 120,
			targetScrollLineAmount = 8,
			targetScrollLineHeight = 20,
			
			--comparison scrolls
			comparisonScrollWidth = 140,
			targetMaxLines = 16,
			targetTooltipLineHeight = 16,
			
			--font settings
			fontSize = 10,
			playerNameSize = 12,
			playerNameYOffset = 12,
			
			--line colors
			lineOnEnterColor = {.85, .85, .85, .5},
			
			--tooltips
			tooltipBorderColor = {.2, .2, .2, .9},
		}
		
		local comparisonLineContrast = {{1, 1, 1, .1}, {1, 1, 1, 0}}
		local latestLinesHighlighted = {}
		local comparisonTooltips = {nextTooltip = 0}
		local comparisonTargetTooltips = {nextTooltip = 0}
		
		local resetTargetComparisonTooltip = function()
			comparisonTargetTooltips.nextTooltip = 0
			for _, tooltip in ipairs (comparisonTargetTooltips) do
				for i = 1, #tooltip.lines do
					local line = tooltip.lines [i]
					line.spellIcon:SetTexture ("")
					line.spellName:SetText ("")
					line.spellAmount:SetText ("")
					line.spellPercent:SetText ("")
					line:Hide()
				end
				
				tooltip:Hide()
				tooltip:ClearAllPoints()
			end
		end
		
		local resetComparisonTooltip = function()
			comparisonTooltips.nextTooltip = 0
			for _, tooltip in ipairs (comparisonTooltips) do
				tooltip:Hide()
				tooltip:ClearAllPoints()
			end
		end
		
		local getTargetComparisonTooltip = function()
			comparisonTargetTooltips.nextTooltip = comparisonTargetTooltips.nextTooltip + 1
			local tooltip = comparisonTargetTooltips [comparisonTargetTooltips.nextTooltip]
			
			if (tooltip) then
				return tooltip
			end
			
			tooltip = CreateFrame ("frame", nil, UIParent, "BackdropTemplate")
			tooltip:SetFrameStrata ("tooltip")
			tooltip:SetSize (1, 1)
			_detalhes.gump:CreateBorder (tooltip)
			tooltip:SetBackdrop ({edgeFile = [[Interface\Buttons\WHITE8X8]], edgeSize = 1, bgFile = [[Interface\AddOns\Details\images\background]], tileSize = 64, tile = true})
			tooltip:SetBackdropColor (.2, .2, .2, .99)
			tooltip:SetBackdropBorderColor (unpack (comparisonFrameSettings.tooltipBorderColor))
			tooltip:SetHeight (77)
			
			local bg_color = {0.5, 0.5, 0.5}
			local bg_texture = [[Interface\AddOns\Details\images\bar_background]]
			local bg_alpha = 1
			local bg_height = 12
			local colors = {{26/255, 26/255, 26/255}, {19/255, 19/255, 19/255}, {26/255, 26/255, 26/255}, {34/255, 39/255, 42/255}, {42/255, 51/255, 60/255}}
			
			--player name label
			tooltip.player_name_label = tooltip:CreateFontString (nil, "overlay", "GameFontHighlightSmall")
			tooltip.player_name_label:SetPoint ("bottomleft", tooltip, "topleft", 1, 2)
			tooltip.player_name_label:SetTextColor (1, .7, .1, .834)
			DetailsFramework:SetFontSize (tooltip.player_name_label, 11)
			
			local name_bg = tooltip:CreateTexture (nil, "artwork")
			name_bg:SetTexture (bg_texture)
			name_bg:SetPoint ("bottomleft", tooltip, "topleft", 0, 1)
			name_bg:SetPoint ("bottomright", tooltip, "topright", 0, 1)
			name_bg:SetHeight (bg_height + 2)
			name_bg:SetAlpha (bg_alpha)
			name_bg:SetVertexColor (unpack (colors[2]))
			
			comparisonTargetTooltips [comparisonTargetTooltips.nextTooltip] = tooltip
			
			tooltip.lines = {}
			
			local lineHeight = comparisonFrameSettings.targetTooltipLineHeight
			local fontSize = 10
			
			for i = 1, comparisonFrameSettings.targetMaxLines do
				local line = CreateFrame ("frame", nil, tooltip, "BackdropTemplate")
				line:SetPoint ("topleft", tooltip, "topleft", 0, -(i-1) * (lineHeight + 1))
				line:SetPoint ("topright", tooltip, "topright", 0, -(i-1) * (lineHeight + 1))
				line:SetHeight (lineHeight)
				
				line:SetBackdrop ({bgFile = [[Interface\Tooltips\UI-Tooltip-Background]], tileSize = 64, tile = true})
				line:SetBackdropColor (0, 0, 0, 0.2)
				
				local spellIcon = line:CreateTexture ("$parentIcon", "overlay")
				spellIcon:SetSize (lineHeight -2 , lineHeight - 2)
				
				local spellName = line:CreateFontString ("$parentName", "overlay", "GameFontNormal")
				local spellAmount = line:CreateFontString ("$parentAmount", "overlay", "GameFontNormal")
				local spellPercent = line:CreateFontString ("$parentPercent", "overlay", "GameFontNormal")
				DetailsFramework:SetFontSize (spellName, fontSize)
				DetailsFramework:SetFontSize (spellAmount, fontSize)
				DetailsFramework:SetFontSize (spellPercent, fontSize)
				
				spellIcon:SetPoint ("left", line, "left", 2, 0)
				spellName:SetPoint ("left", spellIcon, "right", 2, 0)
				spellAmount:SetPoint ("right", line, "right", -2, 0)
				spellPercent:SetPoint ("right", line, "right", -40, 0)
				
				spellName:SetJustifyH ("left")
				spellAmount:SetJustifyH ("right")
				spellPercent:SetJustifyH ("right")
				
				line.spellIcon = spellIcon
				line.spellName = spellName
				line.spellAmount = spellAmount
				line.spellPercent = spellPercent
				
				tooltip.lines [#tooltip.lines+1] = line
				
				if (i % 2 == 0) then
					line:SetBackdropColor (unpack (comparisonLineContrast [1]))
					line.BackgroundColor = comparisonLineContrast [1]
				else
					line:SetBackdropColor (unpack (comparisonLineContrast [2]))
					line.BackgroundColor = comparisonLineContrast [2]
				end
			end
			
			return tooltip
		end
		
		local getComparisonTooltip = function()
			comparisonTooltips.nextTooltip = comparisonTooltips.nextTooltip + 1
			local tooltip = comparisonTooltips [comparisonTooltips.nextTooltip]
			
			if (tooltip) then
				return tooltip
			end
			
			tooltip = CreateFrame ("frame", nil, UIParent, "BackdropTemplate")
			tooltip:SetFrameStrata ("tooltip")
			tooltip:SetSize (1, 1)
			_detalhes.gump:CreateBorder (tooltip)
			tooltip:SetBackdrop ({edgeFile = [[Interface\Buttons\WHITE8X8]], edgeSize = 1, bgFile = [[Interface\AddOns\Details\images\background]], tileSize = 64, tile = true})
			tooltip:SetBackdropColor (0, 0, 0, 1)
			tooltip:SetBackdropBorderColor (unpack (comparisonFrameSettings.tooltipBorderColor))
			tooltip:SetHeight (77)
			
			comparisonTooltips [comparisonTooltips.nextTooltip] = tooltip
			
			--prototype
			local y = -3
			local x_start = 2
			
			local bg_color = {0.5, 0.5, 0.5}
			local bg_texture = [[Interface\AddOns\Details\images\bar_background]]
			local bg_alpha = 1
			local bg_height = 12
			local colors = {{26/255, 26/255, 26/255}, {19/255, 19/255, 19/255}, {26/255, 26/255, 26/255}, {34/255, 39/255, 42/255}, {42/255, 51/255, 60/255}}
			
			local background = tooltip:CreateTexture (nil, "border")
			background:SetTexture ([[Interface\SPELLBOOK\Spellbook-Page-1]])
			background:SetTexCoord (.6, 0.1, 0, 0.64453125)
			background:SetVertexColor (0, 0, 0, 0.2)
			background:SetPoint ("topleft", tooltip, "topleft", 0, 0)
			background:SetPoint ("bottomright", tooltip, "bottomright", 0, 0)
			
			--player name label
			tooltip.player_name_label = tooltip:CreateFontString (nil, "overlay", "GameFontHighlightSmall")
			tooltip.player_name_label:SetPoint ("bottomleft", tooltip, "topleft", 1, 2)
			tooltip.player_name_label:SetTextColor (1, .7, .1, .834)
			DetailsFramework:SetFontSize (tooltip.player_name_label, 11)
			
			local name_bg = tooltip:CreateTexture (nil, "artwork")
			name_bg:SetTexture (bg_texture)
			name_bg:SetPoint ("bottomleft", tooltip, "topleft", 0, 1)
			name_bg:SetPoint ("bottomright", tooltip, "topright", 0, 1)
			name_bg:SetHeight (bg_height + 2)
			name_bg:SetAlpha (bg_alpha)
			name_bg:SetVertexColor (unpack (colors[2]))
			
			--cast line
			tooltip.casts_label = tooltip:CreateFontString (nil, "overlay", "GameFontHighlightSmall")
			tooltip.casts_label:SetPoint ("topleft", tooltip, "topleft", x_start, -2 + (y*0))
			tooltip.casts_label:SetText ("Casts:")
			tooltip.casts_label:SetJustifyH ("left")
			tooltip.casts_label2 = tooltip:CreateFontString (nil, "overlay", "GameFontHighlightSmall")
			tooltip.casts_label2:SetPoint ("topright", tooltip, "topright", -x_start, -2 + (y*0))
			tooltip.casts_label2:SetText ("0")
			tooltip.casts_label2:SetJustifyH ("right")
			tooltip.casts_label3 = tooltip:CreateFontString (nil, "overlay", "GameFontHighlightSmall")
			tooltip.casts_label3:SetPoint ("topright", tooltip, "topright", -x_start - 46, -2 + (y*0))
			tooltip.casts_label3:SetText ("0")
			tooltip.casts_label3:SetJustifyH ("right")
			
			--hits
			tooltip.hits_label = tooltip:CreateFontString (nil, "overlay", "GameFontHighlightSmall")
			tooltip.hits_label:SetPoint ("topleft", tooltip, "topleft", x_start, -14 + (y*1))
			tooltip.hits_label:SetText ("Hits:")
			tooltip.hits_label:SetJustifyH ("left")
			tooltip.hits_label2 = tooltip:CreateFontString (nil, "overlay", "GameFontHighlightSmall")
			tooltip.hits_label2:SetPoint ("topright", tooltip, "topright", -x_start, -14 + (y*1))
			tooltip.hits_label2:SetText ("0")
			tooltip.hits_label2:SetJustifyH ("right")
			tooltip.hits_label3 = tooltip:CreateFontString (nil, "overlay", "GameFontHighlightSmall")
			tooltip.hits_label3:SetPoint ("topright", tooltip, "topright", -x_start - 46, -14 + (y*1))
			tooltip.hits_label3:SetText ("0")
			tooltip.hits_label3:SetJustifyH ("right")
			
			--average
			tooltip.average_label = tooltip:CreateFontString (nil, "overlay", "GameFontHighlightSmall")
			tooltip.average_label:SetPoint ("topleft", tooltip, "topleft", x_start, -26 + (y*2))
			tooltip.average_label:SetText ("Average:")
			tooltip.average_label:SetJustifyH ("left")
			tooltip.average_label2 = tooltip:CreateFontString (nil, "overlay", "GameFontHighlightSmall")
			tooltip.average_label2:SetPoint ("topright", tooltip, "topright", -x_start, -26 + (y*2))
			tooltip.average_label2:SetText ("0")
			tooltip.average_label2:SetJustifyH ("right")
			tooltip.average_label3 = tooltip:CreateFontString (nil, "overlay", "GameFontHighlightSmall")
			tooltip.average_label3:SetPoint ("topright", tooltip, "topright", -x_start - 46, -26 + (y*2))
			tooltip.average_label3:SetText ("0")
			tooltip.average_label3:SetJustifyH ("right")
			
			--critical
			tooltip.crit_label = tooltip:CreateFontString (nil, "overlay", "GameFontHighlightSmall")
			tooltip.crit_label:SetPoint ("topleft", tooltip, "topleft", x_start, -38 + (y*3))
			tooltip.crit_label:SetText ("Critical:")
			tooltip.crit_label:SetJustifyH ("left")
			tooltip.crit_label2 = tooltip:CreateFontString (nil, "overlay", "GameFontHighlightSmall")
			tooltip.crit_label2:SetPoint ("topright", tooltip, "topright", -x_start, -38 + (y*3))
			tooltip.crit_label2:SetText ("0")
			tooltip.crit_label2:SetJustifyH ("right")
			tooltip.crit_label3 = tooltip:CreateFontString (nil, "overlay", "GameFontHighlightSmall")
			tooltip.crit_label3:SetPoint ("topright", tooltip, "topright", -x_start - 46, -38 + (y*3))
			tooltip.crit_label3:SetText ("0")
			tooltip.crit_label3:SetJustifyH ("right")
			
			--uptime
			tooltip.uptime_label = tooltip:CreateFontString (nil, "overlay", "GameFontHighlightSmall")
			tooltip.uptime_label:SetPoint ("topleft", tooltip, "topleft", x_start, -50 + (y*4))
			tooltip.uptime_label:SetText ("Uptime:")
			tooltip.uptime_label:SetJustifyH ("left")
			tooltip.uptime_label2 = tooltip:CreateFontString (nil, "overlay", "GameFontHighlightSmall")
			tooltip.uptime_label2:SetPoint ("topright", tooltip, "topright", -x_start, -50 + (y*4))
			tooltip.uptime_label2:SetText ("0")
			tooltip.uptime_label2:SetJustifyH ("right")
			tooltip.uptime_label3 = tooltip:CreateFontString (nil, "overlay", "GameFontHighlightSmall")
			tooltip.uptime_label3:SetPoint ("topright", tooltip, "topright", -x_start - 46, -50 + (y*4))
			tooltip.uptime_label3:SetText ("0")
			tooltip.uptime_label3:SetJustifyH ("right")
			
			for i = 1, 5 do
				local bg_line1 = tooltip:CreateTexture (nil, "artwork")
				bg_line1:SetTexture (bg_texture)
				bg_line1:SetPoint ("topleft", tooltip, "topleft", 0, -2 + (((i-1) * 12) * -1) + (y * (i-1)) + 2)
				bg_line1:SetPoint ("topright", tooltip, "topright", -0, -2 + (((i-1) * 12) * -1)  + (y * (i-1)) + 2)
				bg_line1:SetHeight (bg_height + 4)
				bg_line1:SetAlpha (bg_alpha)
				bg_line1:SetVertexColor (unpack (colors[i]))
			end
			
			return tooltip
		end
		
		--fill the tooltip for the main player being compared
		--actualPlayerName is the name of the player being compared, playerName can be the name of a pet
		local fillMainSpellTooltip = function (line, rawSpellTable, actualPlayerName, playerName)
			local tooltip = getComparisonTooltip()
			local formatFunc = Details:GetCurrentToKFunction()
			local spellId = rawSpellTable.id
			
			tooltip.player_name_label:SetText (Details:GetOnlyName (actualPlayerName))
			
			local fullPercent = "100%"
			local noData = "-"
			
			--amount of casts
			local castAmount = 0
			local combatObject = Details:GetCombatFromBreakdownWindow()
			local playerMiscObject = combatObject:GetActor (DETAILS_ATTRIBUTE_MISC, playerName)
			
			if (playerMiscObject) then
				castAmount = playerMiscObject.spell_cast and playerMiscObject.spell_cast [spellId] or 0
				tooltip.casts_label2:SetText (fullPercent)
				tooltip.casts_label3:SetText (castAmount)
				DetailsFramework:SetFontColor (tooltip.casts_label2, "gray")
				DetailsFramework:SetFontColor (tooltip.casts_label3, "white")
			else
				tooltip.casts_label2:SetText (noData)
				tooltip.casts_label3:SetText (noData)
				DetailsFramework:SetFontColor (tooltip.casts_label2, "silver")
				DetailsFramework:SetFontColor (tooltip.casts_label3, "silver")
			end
			
			--hit amount
			tooltip.hits_label2:SetText (fullPercent)
			DetailsFramework:SetFontColor (tooltip.hits_label2, "gray")
			tooltip.hits_label3:SetText (rawSpellTable.counter)
			DetailsFramework:SetFontColor (tooltip.hits_label3, "white")
			
			--average
			tooltip.average_label2:SetText (fullPercent)
			DetailsFramework:SetFontColor (tooltip.average_label2, "gray")
			local average = rawSpellTable.total / rawSpellTable.counter
			tooltip.average_label3:SetText (formatFunc (_, average))
			
			--critical strikes
			tooltip.crit_label2:SetText (fullPercent)
			DetailsFramework:SetFontColor (tooltip.crit_label2, "gray")
			tooltip.crit_label3:SetText (rawSpellTable.c_amt)
			
			--uptime
			local uptime = 0
			if (playerMiscObject) then
				local spell = playerMiscObject.debuff_uptime_spells and playerMiscObject.debuff_uptime_spells._ActorTable and playerMiscObject.debuff_uptime_spells._ActorTable [spellId]
				if (spell) then
					local minutos, segundos = floor (spell.uptime / 60), floor (spell.uptime % 60)
					uptime = spell.uptime
					tooltip.uptime_label2:SetText (fullPercent)
					tooltip.uptime_label3:SetText (minutos .. "m " .. segundos .. "s")
					
					DetailsFramework:SetFontColor (tooltip.uptime_label2, "gray")
					DetailsFramework:SetFontColor (tooltip.uptime_label3, "white")
				else
					tooltip.uptime_label2:SetText (noData)
					tooltip.uptime_label3:SetText (noData)
					DetailsFramework:SetFontColor (tooltip.uptime_label2, "gray")
					DetailsFramework:SetFontColor (tooltip.uptime_label3, "gray")
				end
			else
				tooltip.uptime_label2:SetText (noData)
				tooltip.uptime_label3:SetText (noData)
				DetailsFramework:SetFontColor (tooltip.uptime_label2, "gray")
				DetailsFramework:SetFontColor (tooltip.uptime_label3, "gray")
			end
			
			--show tooltip
			tooltip:SetPoint ("bottom", line, "top", 0, 2)
			tooltip:SetWidth (line:GetWidth())
			tooltip:Show()
			
			--highlight line
			line:SetBackdropColor (unpack (comparisonFrameSettings.lineOnEnterColor))
			latestLinesHighlighted [#latestLinesHighlighted + 1] = line
			
			return true, castAmount, rawSpellTable.counter, average, rawSpellTable.c_amt, uptime
		end
		
		local getPercentComparison = function (value1, value2)
			if (value1 == 0 and value2 == 0) then
				return "|c" .. minor .. "0%|r"
			
			elseif (value1 >= value2) then
				local diff = value1 - value2
				local up = diff / value2 * 100
				up = floor (up)
				
				if (up > 999) then
					up = "" .. 999
				end
				
				return "|c" .. minor .. up .. "%|r"
			else
				local diff = value2 - value1
				local down = diff / value1 * 100
				down = floor (down)
				if (down > 999) then
					down = "" .. 999
				end
				
				return "|c" .. plus .. down .. "%|r"
			end		
		end
		
		--fill the tooltip for comparison lines
		--actualPlayerName is the name of the player being compared, playerName can be the name of a pet
		local fillComparisonSpellTooltip = function (line, rawSpellTable, actualPlayerName, playerName, mainCastAmount, mainHitCounter, mainAverageDamage, mainCritAmount, mainAuraUptime)
			local tooltip = getComparisonTooltip()
			local formatFunc = Details:GetCurrentToKFunction()
			local spellId = rawSpellTable.id
			local noData = "-"

			tooltip.player_name_label:SetText (Details:GetOnlyName (actualPlayerName))
			
			--amount of casts	
			local castAmount = 0	
			local combatObject = Details:GetCombatFromBreakdownWindow()			
			local playerMiscObject = combatObject:GetActor (DETAILS_ATTRIBUTE_MISC, playerName)
			
			if (playerMiscObject) then
				castAmount = playerMiscObject.spell_cast and playerMiscObject.spell_cast [spellId] or 0
				tooltip.casts_label2:SetText (getPercentComparison (mainCastAmount, castAmount))
				tooltip.casts_label3:SetText (castAmount)
				DetailsFramework:SetFontColor (tooltip.casts_label2, "white")
				DetailsFramework:SetFontColor (tooltip.casts_label3, "white")
			else
				tooltip.casts_label2:SetText (noData)
				tooltip.casts_label3:SetText (noData)
				DetailsFramework:SetFontColor (tooltip.casts_label2, "silver")
				DetailsFramework:SetFontColor (tooltip.casts_label3, "silver")
			end
			
			--hits
			tooltip.hits_label2:SetText (getPercentComparison (mainHitCounter, rawSpellTable.counter))
			tooltip.hits_label3:SetText (rawSpellTable.counter)
			DetailsFramework:SetFontColor (tooltip.hits_label2, "white")
			DetailsFramework:SetFontColor (tooltip.hits_label3, "white")
			
			--average
			local average = rawSpellTable.total / rawSpellTable.counter
			tooltip.average_label2:SetText (getPercentComparison (mainAverageDamage, average))
			tooltip.average_label3:SetText (formatFunc (_, average))
			DetailsFramework:SetFontColor (tooltip.average_label3, "white")
			DetailsFramework:SetFontColor (tooltip.average_label2, "white")
			
			--critical strikes
			tooltip.crit_label2:SetText (getPercentComparison (mainCritAmount, rawSpellTable.c_amt))
			tooltip.crit_label3:SetText (rawSpellTable.c_amt)
			DetailsFramework:SetFontColor (tooltip.crit_label2, "white")
			DetailsFramework:SetFontColor (tooltip.crit_label2, "white")
			
			--uptime
			local uptime = 0
			if (playerMiscObject) then
				local spell = playerMiscObject.debuff_uptime_spells and playerMiscObject.debuff_uptime_spells._ActorTable and playerMiscObject.debuff_uptime_spells._ActorTable [spellId]
				if (spell) then
					local minutos, segundos = floor (spell.uptime / 60), floor (spell.uptime % 60)
					uptime = spell.uptime
					tooltip.uptime_label2:SetText (getPercentComparison (mainAuraUptime, uptime))
					tooltip.uptime_label3:SetText (minutos .. "m " .. segundos .. "s")
					
					DetailsFramework:SetFontColor (tooltip.uptime_label2, "white")
					DetailsFramework:SetFontColor (tooltip.uptime_label3, "white")
				else
					tooltip.uptime_label2:SetText (noData)
					tooltip.uptime_label3:SetText (noData)
					DetailsFramework:SetFontColor (tooltip.uptime_label2, "gray")
					DetailsFramework:SetFontColor (tooltip.uptime_label3, "gray")
				end
			else
				tooltip.uptime_label2:SetText (noData)
				tooltip.uptime_label3:SetText (noData)
				DetailsFramework:SetFontColor (tooltip.uptime_label2, "gray")
				DetailsFramework:SetFontColor (tooltip.uptime_label3, "gray")
			end
			
			--show tooltip
			tooltip:SetPoint ("bottom", line, "top", 0, 2)
			tooltip:SetWidth (line:GetWidth())
			tooltip:Show()
			
			--highlight line
			line:SetBackdropColor (unpack (comparisonFrameSettings.lineOnEnterColor))
			latestLinesHighlighted [#latestLinesHighlighted + 1] = line
		end
		
		local comparisonLineOnEnter = function (self)
			local scrollFrame = self:GetParent()
			local frame = scrollFrame:GetParent()
			
			if (not frame.GetPlayerInfo) then
				frame = frame:GetParent()
			end
		
			--check if this is a spell or a target line
			if (self.lineType == "mainPlayerSpell" or self.lineType == "comparisonPlayerSpell") then
			
				--get data
				local spellTable = self.spellTable
				local isPet = spellTable.npcId
				local npcId = spellTable.npcId
				local spellId = spellTable.spellId
				
				local mainPlayerObject = frame.GetMainPlayerObject()
				local mainSpellTable = frame.GetMainSpellTable()
				local allComparisonFrames = frame.GetAllComparisonFrames()
				
				local mainFrameScroll = frame.mainFrameScroll
				local playerObject, playerName = frame.GetPlayerInfo (scrollFrame)
				
				--store the spell information from the main tooltip
				local mainHasTooltip, castAmount, hitCounter, averageDamage, critAmount, auraUptime
				
				--iterate on the main player scroll and find the line
				local mainFrameLines = mainFrameScroll:GetFrames()
				for i = 1, #mainFrameLines do 
					local line = mainFrameLines [i]
					if (line.spellTable and line:IsShown()) then
						if (isPet) then
							if (line.spellTable.spellId == spellId and line.spellTable.npcId == npcId) then
								--main line for the hover over spell
								local rawSpellTable = line.spellTable.rawSpellTable
								mainHasTooltip, castAmount, hitCounter, averageDamage, critAmount, auraUptime = fillMainSpellTooltip (line, rawSpellTable, mainPlayerObject:Name(), line.spellTable.originalName)
								break
							end
						else
							if (line.spellTable.spellId == spellId and not line.spellTable.npcId) then
								--main line for the hover over spell
								local rawSpellTable = line.spellTable.rawSpellTable
								mainHasTooltip, castAmount, hitCounter, averageDamage, critAmount, auraUptime = fillMainSpellTooltip (line, rawSpellTable, mainPlayerObject:Name(), mainPlayerObject:Name())
								break
							end
						end
					end
				end
				
				if (mainHasTooltip) then
					--iterate among all other comparison scrolls
					for i = 1, #allComparisonFrames do
						local comparisonFrame = allComparisonFrames [i]
						if (comparisonFrame:IsShown()) then
							local scrollFrame = comparisonFrame.spellsScroll
							local playerObject, playerName = frame.GetPlayerInfo (comparisonFrame)
							
							local frameLines = scrollFrame:GetFrames()
							for i = 1, #frameLines do 
								local line = frameLines [i]
								if (line.spellTable and line:IsShown() and line.spellTable.rawSpellTable) then
									if (isPet) then
										if (line.spellTable.spellId == spellId and line.spellTable.npcId == npcId) then
											--line for the hover over spell in a comparison scroll frame
											local rawSpellTable = line.spellTable.rawSpellTable
											fillComparisonSpellTooltip (line, rawSpellTable, playerName, line.spellTable.originalName, castAmount, hitCounter, averageDamage, critAmount, auraUptime)
										end
									else
										if (line.spellTable.spellId == spellId and not line.spellTable.npcId) then
											--line for the hover over spell in a comparison scroll frame
											local rawSpellTable = line.spellTable.rawSpellTable
											fillComparisonSpellTooltip (line, rawSpellTable, playerName, playerName, castAmount, hitCounter, averageDamage, critAmount, auraUptime)
										end
									end
								end
							end
						end
					end
				end
				
				
				
				
			elseif (self.lineType == "mainPlayerTarget" or self.lineType == "comparisonPlayerTarget") then
				
				local targetName = self.targetTable.originalName
				local attribute = Details:GetDisplayTypeFromBreakdownWindow()
				
				--build a list of spells used by the main actor
				local mainPlayerObject = frame.GetMainPlayerObject()
				local damageDoneBySpell = {}
				
				--find the main line
				--iterate on the main player scroll and find the line
				local mainLine
				local mainFrameScroll = frame.targetFrameScroll
				local mainFrameLines = mainFrameScroll:GetFrames()
				
				for i = 1, #mainFrameLines do 
					local line = mainFrameLines [i]
					if (line.targetTable and line:IsShown()) then
						if (line.targetTable.originalName == targetName) then
							mainLine = line
							break
						end
					end
				end
				
				if (not mainLine) then
					return
				end
				
				--spells
				for spellId, spellTable in pairs (mainPlayerObject:GetActorSpells()) do
					local damageOnTarget = spellTable.targets [targetName]
					if (damageOnTarget and damageOnTarget > 0) then
						damageDoneBySpell [#damageDoneBySpell + 1] = {spellTable, damageOnTarget, mainPlayerObject:Name(), mainPlayerObject, false}
					end
				end
				
				--pets
				for _, petName in ipairs (mainPlayerObject:Pets()) do
					local petObject = Details:GetCombatFromBreakdownWindow():GetActor (attribute, petName)
					if (petObject) then
						for spellId, spellTable in pairs (petObject:GetActorSpells()) do
							local damageOnTarget = spellTable.targets [targetName]
							if (damageOnTarget and damageOnTarget > 0) then
								damageDoneBySpell [#damageDoneBySpell + 1] = {spellTable, damageOnTarget, petName, petObject, DetailsFramework:GetNpcIdFromGuid (petObject.serial)}
							end
						end
					end
				end
				
				table.sort (damageDoneBySpell, DetailsFramework.SortOrder2)

				local tooltip = getTargetComparisonTooltip()
				local formatFunc = Details:GetCurrentToKFunction()
				local mainHasTooltip = false
				tooltip.player_name_label:SetText (mainPlayerObject:GetDisplayName())
				
				for i = 1, #damageDoneBySpell do
					local damageTable = damageDoneBySpell [i]
					local spellTable = damageTable [1]
					local damageDone = damageTable [2]
					local actorName = damageTable [3]
					local actorObject = damageTable [4]
					local npcId = damageTable [5]
					
					local spellName, _, spellIcon = Details.GetSpellInfo (spellTable.id)
					
					local line = tooltip.lines [i]
					if (not line) then
						break
					end
					
					if (npcId) then
						spellName = spellName .. " (" .. comparisonFrameSettings.petColor .. actorName:gsub (" <.*", "") .. "|r)"
					end
					
					line.spellName:SetText (spellName)
					DetailsFramework:TruncateText (line.spellName, mainFrameScroll:GetWidth() - 110)
					
					line.spellIcon:SetTexture (spellIcon)
					line.spellIcon:SetTexCoord (.1, .9, .1, .9)
					line.spellAmount:SetText ("100%")
					DetailsFramework:SetFontColor (line.spellAmount, "gray")
					line.spellPercent:SetText (formatFunc (_, damageDone))
					
					line:Show()
					mainHasTooltip = true
				end
				
				--comparison
				if (mainHasTooltip) then
					local allComparisonFrames = frame.GetAllComparisonFrames()
					local combatObject = Details:GetCombatFromBreakdownWindow()
					
					--iterate among all other comparison scrolls
					for i = 1, #allComparisonFrames do
						local comparisonFrame = allComparisonFrames [i]
						if (comparisonFrame:IsShown()) then
							local scrollFrame = comparisonFrame.targetsScroll
							local playerObject, playerName = frame.GetPlayerInfo (comparisonFrame)
							
							local targetLines = scrollFrame:GetFrames()
							for o = 1, #targetLines do
								local line = targetLines [o]
								if (line and line.targetTable and line:IsShown()) then
									if (line.targetTable.originalName == targetName) then
										
										--get a tooltip for this actor
										local actorTooltip = getTargetComparisonTooltip()
										
										actorTooltip:SetPoint ("bottom", line, "top", 0, 2)
										actorTooltip:SetWidth (line:GetWidth())
										actorTooltip:SetHeight (min (comparisonFrameSettings.targetMaxLines, #damageDoneBySpell) * comparisonFrameSettings.targetTooltipLineHeight + comparisonFrameSettings.targetMaxLines)
										actorTooltip:Show()
										
										actorTooltip.player_name_label:SetText (playerObject:GetDisplayName())
										
										--highlight line
										line:SetBackdropColor (unpack (comparisonFrameSettings.lineOnEnterColor))
										latestLinesHighlighted [#latestLinesHighlighted + 1] = line
										
										--iterate among all spells in the first tooltip and fill here
										--if is a pet line, need to get the data from the player pet instead
										
										
										for a = 1, #damageDoneBySpell do
											local damageTable = damageDoneBySpell [a]
											local spellTable = damageTable [1]
											local damageDone = damageTable [2]
											local actorName = damageTable [3]
											local actorObject = damageTable [4]
											local npcId = damageTable [5]
											
											local foundSpell
											
											-- i is also the tooltip line index
											
											if (not npcId) then
												local spellObject = playerObject:GetSpell (spellTable.id)
												if (spellObject) then
													local damageOnTarget = spellObject.targets [targetName] or 0
													if (damageOnTarget > 0) then
														--this actor did damage on this target, add into the tooltip
														local tooltipLine = actorTooltip.lines [a]
														if (not tooltipLine) then
															break
														end
														
														local spellName, _, spellIcon = Details.GetSpellInfo (spellTable.id)
														
														tooltipLine.spellName:SetText ("")
														tooltipLine.spellIcon:SetTexture (spellIcon)
														tooltipLine.spellIcon:SetTexCoord (.1, .9, .1, .9)
														
														-- calculate percent 
														local mainSpellDamageOnTarget = 0
														-- find this spell in the main actor table
														for u = 1, #damageDoneBySpell do
															local mainSpell = damageDoneBySpell [u]

															local spellTableMain = damageTable [1]
															local damageDoneMain = damageTable [2]
															local actorNameMain = damageTable [3]
															local actorObjectMain = damageTable [4]
															local npcIdMain = damageTable [5]
															
															if (not npcIdMain and spellTableMain.id == spellObject.id) then
																--found the spell in the main table
																mainSpellDamageOnTarget = damageDoneMain
																break
															end
														end
														
														tooltipLine.spellAmount:SetText (getPercentComparison (mainSpellDamageOnTarget, damageOnTarget))
														tooltipLine.spellPercent:SetText (formatFunc (_, damageOnTarget))
														
														tooltipLine:Show()
														foundSpell = true
													end
												end
											else
												--iterate among all pets the player has and find one with the same npcId
												for _, petName in ipairs (playerObject:Pets()) do
													local petObject = combatObject:GetActor (attribute, petName)
													if (petObject) then
														local petNpcId = DetailsFramework:GetNpcIdFromGuid (petObject.serial)
														if (petNpcId and petNpcId == npcId) then
															--found the correct pet
															local spellObject = petObject:GetSpell (spellTable.id)
															if (spellObject) then
																local damageOnTarget = spellObject.targets [targetName] or 0
																if (damageOnTarget > 0) then
																	--this pet did damage on this target, add into the tooltip

																	local tooltipLine = actorTooltip.lines [a]
																	if (not tooltipLine) then
																		break
																	end
																	
																	-- calculate percent 
																	local mainSpellDamageOnTarget = 0
																	-- find this spell in the main actor table
																	for u = 1, #damageDoneBySpell do
																		local mainSpell = damageDoneBySpell [u]

																		local spellTableMain = damageTable [1]
																		local damageDoneMain = damageTable [2]
																		local actorNameMain = damageTable [3]
																		local actorObjectMain = damageTable [4]
																		local npcIdMain = damageTable [5]
																		
																		if (npcIdMain and npcIdMain == petNpcId and spellTableMain.id == spellObject.id) then
																			--found the spell in the main table
																			mainSpellDamageOnTarget = damageDoneMain
																			break
																		end
																	end
																	
																	local spellName, _, spellIcon = Details.GetSpellInfo (spellTable.id)
																	
																	tooltipLine.spellName:SetText ("")
																	tooltipLine.spellIcon:SetTexture (spellIcon)
																	tooltipLine.spellIcon:SetTexCoord (.1, .9, .1, .9)
																	tooltipLine.spellAmount:SetText (getPercentComparison (mainSpellDamageOnTarget, damageOnTarget))
																	tooltipLine.spellPercent:SetText (formatFunc (_, damageOnTarget))
																	
																	tooltipLine:Show()
																	foundSpell = true
																end
															end
														end
													end
												end

											end
											
											if (not foundSpell) then
												--add an empty line in the tooltip
												local tooltipLine = actorTooltip.lines [a]
												if (tooltipLine) then
													tooltipLine:Show()
												end
											end
										end
										
										--break the loop to find the correct line
										break
									end
								end
							end
						end
					end
				end
				
				--highlight line
				mainLine:SetBackdropColor (unpack (comparisonFrameSettings.lineOnEnterColor))
				latestLinesHighlighted [#latestLinesHighlighted + 1] = mainLine
				
				tooltip:SetPoint ("bottom", mainLine, "top", 0, 2)
				tooltip:SetWidth (mainLine:GetWidth())
				tooltip:SetHeight (min (comparisonFrameSettings.targetMaxLines, #damageDoneBySpell) * comparisonFrameSettings.targetTooltipLineHeight + comparisonFrameSettings.targetMaxLines)
				tooltip:Show()
			end
		end
		
		local comparisonLineOnLeave = function (self)
			for i = #latestLinesHighlighted, 1, -1 do
				local line = latestLinesHighlighted [i]
				line:SetBackdropColor (unpack (line.BackgroundColor))
				tremove (latestLinesHighlighted, i)
			end
			
			resetComparisonTooltip()
			resetTargetComparisonTooltip()
		end

		local playerComparisonCreate = function (tab, frame)
			
			frame.isComparisonTab = true
			
			--regular functions for comparison API
			function frame.GetMainPlayerName()
				return frame.mainPlayerObject:Name()
			end
			function frame.GetMainPlayerObject()
				return frame.mainPlayerObject
			end
			
			function frame.GetMainSpellTable()
				return frame.mainSpellTable
			end
			function frame.GetMainTargetTable()
				return frame.mainTargetTable
			end
			
			function frame.GetAllComparisonFrames()
				return frame.comparisonFrames
			end
			
			function frame.GetPlayerInfo (f)
				if (f.mainPlayerObject) then
					return f.mainPlayerObject, f.mainPlayerObject:Name()
				else
					return f.playerObject, f.playerObject:Name()
				end
			end
			
			--main player spells (scroll box with the spells the player used)
			local mainPlayerCreateline = function (self, index)
				local lineHeight = self.lineHeight
				local lineWidth = self.scrollWidth
				local fontSize = self.fontSize
				
				local line = CreateFrame ("button", "$parentLine" .. index, self, "BackdropTemplate")
				line:SetPoint ("topleft", self, "topleft", 1, -((index-1) * (lineHeight+1)))
				line:SetSize (lineWidth -2, lineHeight)
				
				line:SetScript ("OnEnter", comparisonLineOnEnter)
				line:SetScript ("OnLeave", comparisonLineOnLeave)
				
				line:SetBackdrop ({bgFile = [[Interface\Tooltips\UI-Tooltip-Background]], tileSize = 64, tile = true})
				line:SetBackdropColor (0, 0, 0, 0.2)
				
				local spellIcon = line:CreateTexture ("$parentIcon", "overlay")
				spellIcon:SetSize (lineHeight -2 , lineHeight - 2)
				
				local spellName = line:CreateFontString ("$parentName", "overlay", "GameFontNormal")
				local spellAmount = line:CreateFontString ("$parentAmount", "overlay", "GameFontNormal")
				local spellPercent = line:CreateFontString ("$parentPercent", "overlay", "GameFontNormal")
				DetailsFramework:SetFontSize (spellName, fontSize)
				DetailsFramework:SetFontSize (spellAmount, fontSize)
				DetailsFramework:SetFontSize (spellPercent, fontSize)
				
				spellIcon:SetPoint ("left", line, "left", 2, 0)
				spellName:SetPoint ("left", spellIcon, "right", 2, 0)
				spellAmount:SetPoint ("right", line, "right", -2, 0)
				spellPercent:SetPoint ("right", line, "right", -40, 0)
				
				spellName:SetJustifyH ("left")
				spellAmount:SetJustifyH ("right")
				spellPercent:SetJustifyH ("right")
				
				line.spellIcon = spellIcon
				line.spellName = spellName
				line.spellAmount = spellAmount
				line.spellPercent = spellPercent
				
				return line
			end

			--the refresh function receives an already prepared table and just update the lines
			local mainPlayerRefreshSpellScroll = function (self, data, offset, totalLines)
				for i = 1, totalLines do
					local index = i + offset
					local spellTable = data [index]
					
					if (spellTable) then
						local line = self:GetLine (i)
						
						--store the line into the spell table
						spellTable.line = line
						line.spellTable = spellTable
						
						local spellId = spellTable.spellId
						local spellName = spellTable.spellName
						local spellIcon = spellTable.spellIcon
						local amountDone = spellTable.amount
						
						line.spellId = spellId
						
						line.spellIcon:SetTexture (spellIcon)
						line.spellIcon:SetTexCoord (.1, .9, .1, .9)
						
						line.spellName:SetText (spellName)
						DetailsFramework:TruncateText (line.spellName, line:GetWidth() - 70)
						
						local formatFunc = Details:GetCurrentToKFunction()
						line.spellAmount:SetText (formatFunc (_, amountDone))

						if (i % 2 == 0) then
							line:SetBackdropColor (unpack (comparisonLineContrast [1]))
							line.BackgroundColor = comparisonLineContrast [1]
						else
							line:SetBackdropColor (unpack (comparisonLineContrast [2]))
							line.BackgroundColor = comparisonLineContrast [2]
						end
					end
				end
			end
			
			local mainPlayerRefreshTargetScroll = function (self, data, offset, totalLines)
				for i = 1, totalLines do
					local index = i + offset
					local targetTable = data [index]
					
					if (targetTable) then
						local line = self:GetLine (i)
						
						--store the line into the target table
						targetTable.line = line
						line.targetTable = targetTable
						
						local targetName = targetTable.targetName
						local amountDone = targetTable.amount
						
						line.targetName = targetName
						
						line.spellIcon:SetTexture ("") --todo - fill this texture
						line.spellIcon:SetTexCoord (.1, .9, .1, .9)
						
						line.spellName:SetText (targetName)
						DetailsFramework:TruncateText (line.spellName, line:GetWidth() - 50)
						
						local formatFunc = Details:GetCurrentToKFunction()
						line.spellAmount:SetText (formatFunc (_, amountDone))

						if (i % 2 == 0) then
							line:SetBackdropColor (unpack (comparisonLineContrast [1]))
							line.BackgroundColor = comparisonLineContrast [1]
						else
							line:SetBackdropColor (unpack (comparisonLineContrast [2]))
							line.BackgroundColor = comparisonLineContrast [2]
						end
					end
				end
			end
			
			--main player spells scroll
			local mainPlayerFrameScroll = DetailsFramework:CreateScrollBox (frame, "$parentComparisonMainPlayerSpellsScroll", mainPlayerRefreshSpellScroll, {}, comparisonFrameSettings.mainScrollWidth, comparisonFrameSettings.spellScrollHeight, comparisonFrameSettings.spellLineAmount, comparisonFrameSettings.spellLineHeight)
			mainPlayerFrameScroll:SetPoint ("topleft", frame, "topleft", 5, -30)
			mainPlayerFrameScroll.lineHeight = comparisonFrameSettings.spellLineHeight
			mainPlayerFrameScroll.scrollWidth = comparisonFrameSettings.mainScrollWidth
			mainPlayerFrameScroll.fontSize = comparisonFrameSettings.fontSize
			
			mainPlayerFrameScroll:HookScript ("OnVerticalScroll", function (self, offset) 
				frame.RefreshAllComparisonScrollFrames()
			end)			
			
			--create lines
			for i = 1, comparisonFrameSettings.spellLineAmount do 
				local line = mainPlayerFrameScroll:CreateLine (mainPlayerCreateline)
				line.lineType = "mainPlayerSpell"
			end
			DetailsFramework:ReskinSlider (mainPlayerFrameScroll)
			
			frame.mainFrameScroll = mainPlayerFrameScroll
			
			--main player targets (scroll box with enemies the player applied damage)
			local mainTargetFrameScroll = DetailsFramework:CreateScrollBox (frame, "$parentComparisonMainPlayerTargetsScroll", mainPlayerRefreshTargetScroll, {}, comparisonFrameSettings.mainScrollWidth, comparisonFrameSettings.targetScrollHeight, comparisonFrameSettings.targetScrollLineAmount, comparisonFrameSettings.targetScrollLineHeight)
			mainTargetFrameScroll:SetPoint ("topleft", mainPlayerFrameScroll, "bottomleft", 0, -20)
			mainTargetFrameScroll.lineHeight = comparisonFrameSettings.targetScrollLineHeight
			mainTargetFrameScroll.scrollWidth = comparisonFrameSettings.mainScrollWidth
			mainTargetFrameScroll.fontSize = comparisonFrameSettings.fontSize
			
			--create lines
			for i = 1, comparisonFrameSettings.targetScrollLineAmount do 
				local line = mainTargetFrameScroll:CreateLine (mainPlayerCreateline)
				line.lineType = "mainPlayerTarget"
			end
			DetailsFramework:ReskinSlider (mainTargetFrameScroll)
			
			frame.targetFrameScroll = mainTargetFrameScroll

			--main player name
			frame.mainPlayerName = DetailsFramework:CreateLabel (mainPlayerFrameScroll)
			frame.mainPlayerName:SetPoint ("topleft", mainPlayerFrameScroll, "topleft", 2, comparisonFrameSettings.playerNameYOffset)
			frame.mainPlayerName.fontsize = comparisonFrameSettings.playerNameSize
			
			--create the framework for the comparing players
			local settings = {
				--comparison frame
				height = 600,
			}
			
			frame.comparisonFrames = {}
			frame.comparisonScrollFrameIndex = 0
			
			function frame.ResetComparisonFrames()
				frame.comparisonScrollFrameIndex = 0
				for _, comparisonFrame in ipairs (frame.comparisonFrames) do
					comparisonFrame:Hide()
					comparisonFrame.playerObject = nil
					comparisonFrame.spellsScroll.playerObject = nil
				end
			end
			
			local comparisonPlayerRefreshTargetScroll  = function (self, data, offset, totalLines)
				offset = FauxScrollFrame_GetOffset (mainPlayerFrameScroll)
				
				for i = 1, totalLines do
					local index = i + offset
					local targetTable = data [index]
					
					if (targetTable) then
						local line = self:GetLine (i)
						line:SetWidth (comparisonFrameSettings.comparisonScrollWidth - 2)
						
						--store the line into the target table
						targetTable.line = line
						line.targetTable = targetTable
						
						line.spellIcon:SetTexture ("")
						
						local mainPlayerAmount = targetTable.mainTargetAmount
						local amountDone = targetTable.amount
						
						if (mainPlayerAmount) then
							local formatFunc = Details:GetCurrentToKFunction()
							
							if (mainPlayerAmount > amountDone) then
								local diff = mainPlayerAmount - amountDone
								local up = diff / amountDone * 100
								up = floor (up)
								if (up > 999) then
									up = "" .. 999
								end
								
								line.spellPercent:SetText ("|c" .. minor .. up .. "%|r")
							else
								local diff = amountDone - mainPlayerAmount
								local down = diff / mainPlayerAmount * 100
								down = floor (down)
								if (down > 999) then
									down = "" .. 999
								end
								
								line.spellPercent:SetText ("|c" .. plus .. down .. "%|r")
							end
							
							line.spellAmount:SetText (formatFunc (_, amountDone))
						else
							line.spellPercent:SetText ("")
							line.spellAmount:SetText ("")
						end

						if (i % 2 == 0) then
							line:SetBackdropColor (unpack (comparisonLineContrast [1]))
							line.BackgroundColor = comparisonLineContrast [1]
						else
							line:SetBackdropColor (unpack (comparisonLineContrast [2]))
							line.BackgroundColor = comparisonLineContrast [2]
						end
					end
				end
			end
			
			--refresh a spell scroll on a comparison frame
			local comparisonPlayerRefreshSpellScroll = function (self, data, offset, totalLines)
			
				offset = FauxScrollFrame_GetOffset (mainPlayerFrameScroll)
			
				for i = 1, totalLines do
					local index = i + offset
					local spellTable = data [index]
					
					if (spellTable) then
						local line = self:GetLine (i)
						line:SetWidth (comparisonFrameSettings.comparisonScrollWidth - 2)
						
						--store the line into the spell table
						spellTable.line = line
						line.spellTable = spellTable
						
						local spellId = spellTable.spellId
						local spellName = spellTable.spellName
						local spellIcon = spellTable.spellIcon
						local amountDone = spellTable.amount
						
						line.spellId = spellId
						
						line.spellIcon:SetTexture (spellIcon)
						line.spellIcon:SetTexCoord (.1, .9, .1, .9)
						
						line.spellName:SetText ("") --won't show the spell name, only the icon
						
						local percent = 1
						
						local mainFrame = self:GetParent().mainPlayer
						local mainSpellTable = self:GetParent().mainSpellTable
						local mainPlayerAmount = spellTable.mainSpellAmount
						
						if (mainPlayerAmount) then
							local formatFunc = Details:GetCurrentToKFunction()
							
							if (mainPlayerAmount > amountDone) then
								local diff = mainPlayerAmount - amountDone
								local up = diff / amountDone * 100
								up = floor (up)
								if (up > 999) then
									up = "" .. 999
								end
								
								line.spellPercent:SetText ("|c" .. minor .. up .. "%|r")
							else
								local diff = amountDone - mainPlayerAmount
								local down = diff / mainPlayerAmount * 100
								down = floor (down)
								if (down > 999) then
									down = "" .. 999
								end
								
								line.spellPercent:SetText ("|c" .. plus .. down .. "%|r")
							end
							
							line.spellAmount:SetText (formatFunc (_, amountDone))
						else
							line.spellPercent:SetText ("")
							line.spellAmount:SetText ("")
						end

						if (i % 2 == 0) then
							line:SetBackdropColor (unpack (comparisonLineContrast [1]))
							line.BackgroundColor = comparisonLineContrast [1]
						else
							line:SetBackdropColor (unpack (comparisonLineContrast [2]))
							line.BackgroundColor = comparisonLineContrast [2]
						end
					end
				end
			end			
			
			function frame.RefreshAllComparisonScrollFrames()
				for _, comparisonFrame in ipairs (frame.comparisonFrames) do
					comparisonFrame.spellsScroll:Refresh()
				end
			end
			
			--get a frame which has two scrolls, one for spells and another for targets
			function frame.GetComparisonScrollFrame()
				frame.comparisonScrollFrameIndex = frame.comparisonScrollFrameIndex + 1
				
				local comparisonFrame = frame.comparisonFrames [frame.comparisonScrollFrameIndex]
				if (comparisonFrame) then
					comparisonFrame:Show()
					return comparisonFrame
				end
				
				local newComparisonFrame = CreateFrame ("frame", "DetailsComparisonFrame" .. frame.comparisonScrollFrameIndex, frame, "BackdropTemplate")
				frame.comparisonFrames [frame.comparisonScrollFrameIndex] = newComparisonFrame
				newComparisonFrame:SetSize (comparisonFrameSettings.comparisonScrollWidth, settings.height)
				
				if (frame.comparisonScrollFrameIndex == 1) then
					newComparisonFrame:SetPoint ("topleft", mainPlayerFrameScroll, "topright", 30, 0)
				else
					newComparisonFrame:SetPoint ("topleft", frame.comparisonFrames [frame.comparisonScrollFrameIndex - 1], "topright", 10, 0)
				end
				
				--create the spell comparison scroll
					--player name
					newComparisonFrame.playerName = DetailsFramework:CreateLabel (newComparisonFrame)
					newComparisonFrame.playerName:SetPoint ("topleft", newComparisonFrame, "topleft", 2, comparisonFrameSettings.playerNameYOffset)
					newComparisonFrame.playerName.fontsize = comparisonFrameSettings.playerNameSize
					
					--spells scroll
					local spellsScroll = DetailsFramework:CreateScrollBox (newComparisonFrame, "$parentComparisonPlayerSpellsScroll", comparisonPlayerRefreshSpellScroll, {}, comparisonFrameSettings.comparisonScrollWidth, comparisonFrameSettings.spellScrollHeight, comparisonFrameSettings.spellLineAmount, comparisonFrameSettings.spellLineHeight)
					spellsScroll:SetPoint ("topleft", newComparisonFrame, "topleft", 0, 0)
					spellsScroll.lineHeight = comparisonFrameSettings.spellLineHeight
					spellsScroll.scrollWidth = comparisonFrameSettings.mainScrollWidth
					spellsScroll.fontSize = comparisonFrameSettings.fontSize
					_G [spellsScroll:GetName() .. "ScrollBar"]:Hide()
					
						--create lines
						for i = 1, comparisonFrameSettings.spellLineAmount do 
							local line = spellsScroll:CreateLine (mainPlayerCreateline)
							line.lineType = "comparisonPlayerSpell"
						end
						DetailsFramework:ReskinSlider (spellsScroll)
						
						newComparisonFrame.spellsScroll = spellsScroll
					
					--targets scroll
					local targetsScroll = DetailsFramework:CreateScrollBox (newComparisonFrame, "$parentComparisonPlayerTargetsScroll", comparisonPlayerRefreshTargetScroll, {}, comparisonFrameSettings.comparisonScrollWidth, comparisonFrameSettings.targetScrollHeight, comparisonFrameSettings.targetScrollLineAmount, comparisonFrameSettings.targetScrollLineHeight)
					targetsScroll:SetPoint ("topleft", newComparisonFrame, "topleft", 0, -comparisonFrameSettings.spellScrollHeight - 20)
					targetsScroll.lineHeight = comparisonFrameSettings.spellLineHeight
					targetsScroll.scrollWidth = comparisonFrameSettings.mainScrollWidth
					targetsScroll.fontSize = comparisonFrameSettings.fontSize
					_G [targetsScroll:GetName() .. "ScrollBar"]:Hide()
					
						--create lines
						for i = 1, comparisonFrameSettings.targetScrollLineAmount do 
							local line = targetsScroll:CreateLine (mainPlayerCreateline)
							line.lineType = "comparisonPlayerTarget"
						end
						DetailsFramework:ReskinSlider (targetsScroll)
						
						newComparisonFrame.targetsScroll = targetsScroll
					
				return newComparisonFrame
			end
			
		end
		
		local buildSpellAndTargetTables = function (player, combat, attribute)
			--build the spell list for the main player including pets
			local allPlayerSpells = player:GetActorSpells()
			local resultSpellTable = {}
			
			for spellId, spellTable in pairs (allPlayerSpells) do
				local spellName, _, spellIcon = Details.GetSpellInfo (spellId)
				resultSpellTable [#resultSpellTable + 1] = {spellId, spellTable.total, spellName = spellName, spellIcon = spellIcon, spellId = spellId, amount = spellTable.total, rawSpellTable = spellTable}
			end
			
			--pets
			for _, petName in ipairs (player:Pets()) do
				local petObject = combat:GetActor (attribute, petName)
				if (petObject) then
					local allPetSpells = petObject:GetActorSpells()
					local petNpcId = DetailsFramework:GetNpcIdFromGuid (petObject.serial)
					for spellId, spellTable in pairs (allPetSpells) do
						local spellName, _, spellIcon = Details.GetSpellInfo (spellId)
						spellName = spellName .. " (" .. comparisonFrameSettings.petColor .. (Details:RemoveOwnerName (petName)) .. "|r)"
						resultSpellTable [#resultSpellTable + 1] = {spellId, spellTable.total, petName, petNpcId or 0, npcId = petNpcId or 0, spellName = spellName, spellIcon = spellIcon, spellId = spellId, amount = spellTable.total, originalName = petName, rawSpellTable = spellTable}
					end
				end
			end
			
			table.sort (resultSpellTable, DetailsFramework.SortOrder2)
			
			--build the target list for the main player
			local resultTargetTable = {}
			for targetName, amountDone in pairs (player.targets) do
				resultTargetTable [#resultTargetTable + 1] = {targetName, amountDone, targetName = Details:RemoveOwnerName (targetName), amount = amountDone, originalName = targetName, rawPlayerObject = player}
			end
			
			table.sort (resultTargetTable, DetailsFramework.SortOrder2)
			
			return resultSpellTable, resultTargetTable
		end
		
		--main tab function to be executed when the tab is shown
		local playerComparisonFill = function (tab, player, combat)
			local frame = tab.frame
			
			--update player name
			frame.mainPlayerName.text = player:GetDisplayName()
			
			frame.mainPlayerObject = player
			frame.mainFrameScroll.mainPlayerObject = player
			
			--reset the comparison scroll frame
			frame.ResetComparisonFrames()
			resetComparisonTooltip()
			resetTargetComparisonTooltip()
			
			local attribute = Details:GetDisplayTypeFromBreakdownWindow()
			local resultSpellTable, resultTargetTable = buildSpellAndTargetTables (player, combat, attribute)

			--update the two main scroll frames
			frame.mainFrameScroll:SetData (resultSpellTable)
			frame.mainFrameScroll:Refresh()
			
			frame.targetFrameScroll:SetData (resultTargetTable)
			frame.targetFrameScroll:Refresh()
			
			frame.mainSpellTable = resultSpellTable
			frame.mainTargetTable = resultTargetTable
			
			--search the combat for players with the same spec and build a scroll frame for them
			--to keep consistency, sort these players with the max amount of damage or healing they have done
			local actorContainer = combat:GetContainer (attribute)
			local playersWithSameSpec = {}
			
			for _, playerObject in actorContainer:ListActors() do
				if (playerObject:IsPlayer() and playerObject.spec == player.spec and playerObject.serial ~= player.serial) then
					playersWithSameSpec [#playersWithSameSpec + 1] = {playerObject, playerObject.total}
				end
			end
			
			table.sort (playersWithSameSpec, DetailsFramework.SortOrder2)
			
			frame.comparisonSpellTable = {}
			frame.comparisonTargetTable = {}
			
			for i = 1, #playersWithSameSpec do
				--other player with the same spec
				local playerObject = playersWithSameSpec[i][1]
				local otherPlayerSpellTable, otherPlayerTargetTable = buildSpellAndTargetTables (playerObject, combat, attribute)
				
				frame.comparisonSpellTable [#frame.comparisonSpellTable + 1] = otherPlayerSpellTable
				frame.comparisonTargetTable [#frame.comparisonTargetTable + 1] = otherPlayerTargetTable
				
				local comparisonFrame = frame.GetComparisonScrollFrame()
				comparisonFrame.mainPlayer = player
				comparisonFrame.mainSpellTable = resultSpellTable
				comparisonFrame.mainTargetTable = resultTargetTable
				
				comparisonFrame.playerObject = playerObject
				comparisonFrame.playerName.text = playerObject:GetDisplayName()
				
				--iterate among spells of the main player and check if the spell exists on this player
					local otherPlayerResultSpellTable = {}
					
					for i = 1, #resultSpellTable do
						local spellTable = resultSpellTable [i]
						
						local found = false
						
						--iterate amont spell of the other player which the main player is being compared
						for o = 1, #otherPlayerSpellTable do
							local otherSpellTable = otherPlayerSpellTable[o]
						
							--check if this is a pet
							if (spellTable.npcId) then
								--match the npcId before match the spellId
								if (otherSpellTable.npcId == spellTable.npcId) then
									if (otherSpellTable.spellId == spellTable.spellId) then
										found = true
										--insert the amount of the main spell in the table
										otherSpellTable.mainSpellAmount = spellTable.amount
										otherPlayerResultSpellTable [#otherPlayerResultSpellTable+1] = otherSpellTable
										break
									end
								end
							else
								if (otherSpellTable.spellId == spellTable.spellId) then
									found = true
									--insert the amount of the main spell in the table
									otherSpellTable.mainSpellAmount = spellTable.amount
									otherPlayerResultSpellTable [#otherPlayerResultSpellTable+1] = otherSpellTable
									break
								end
							end
						end
						
						if (not found) then
							otherPlayerResultSpellTable [#otherPlayerResultSpellTable+1] = {0, 0, spellName = "", spellIcon = "", spellId = 0, amount = 0}
						end
					end

					--call the update function
					comparisonFrame.spellsScroll.playerObject = playerObject
					comparisonFrame.spellsScroll:SetData (otherPlayerResultSpellTable)
					comparisonFrame.spellsScroll:Refresh()
				
				--iterate among targets of the main player and check if the target exists on this player
					local otherPlayerResultTargetsTable = {}
					
					for i = 1, #resultTargetTable do
						local targetTable = resultTargetTable [i]
						
						local found = false
						
						--iterate amont targets of the other player which the main player is being compared
						for o = 1, #otherPlayerTargetTable do
							local otherTargetTable = otherPlayerTargetTable[o]
						
							if (otherTargetTable.originalName == targetTable.originalName) then
								found = true
								--insert the amount of the main spell in the table
								otherTargetTable.mainTargetAmount = targetTable.amount
								otherPlayerResultTargetsTable [#otherPlayerResultTargetsTable+1] = otherTargetTable
								break
							end
						end
						
						if (not found) then
							otherPlayerResultTargetsTable [#otherPlayerResultTargetsTable+1] = {"", 0, targetName = "", amount = 0, originalName = ""}
						end
					end

					--call the update function
					comparisonFrame.targetsScroll:SetData (otherPlayerResultTargetsTable)
					comparisonFrame.targetsScroll:Refresh()
				
			end
			

		end
		
		local iconTableCompare = {
			texture = [[Interface\AddOns\Details\images\icons]],
			--coords = {363/512, 381/512, 0/512, 17/512},
			coords = {383/512, 403/512, 0/512, 15/512},
			width = 16,
			height = 14,
		}

		_detalhes:CreatePlayerDetailsTab ("New Compare", "Compare2", --[1] tab name [2] localized name
			function (tabOBject, playerObject)  --[2] condition
				
				local attribute = Details:GetDisplayTypeFromBreakdownWindow()
				local combat = Details:GetCombatFromBreakdownWindow()
				
				if (attribute > 2) then
					return false
				end
				
				local playerSpec = playerObject.spec or "no-spec"
				local playerSerial = playerObject.serial
				local showTab = false
				
				for index, actor in _ipairs (combat [attribute]._ActorTable) do 
					if (actor.spec == playerSpec and actor.serial ~= playerSerial) then
						showTab = true
						break
					end
				end
				
				if (showTab) then
					return true
				end
				
				--return false
				return true --debug?
			end, 
			
			playerComparisonFill, --[3] fill function
			
			nil, --[4] onclick
			
			playerComparisonCreate, --[5] oncreate
			iconTableCompare --icon table
		)
	end
	
---------------------------------------------------------------------------------------------------------------------------------------
	
	function aBreakdown.InstallNewSpellBreakdown()
		
		--todo
		--sort order when clicking in the header tab
		--hovering over tooltip showing more details about the spell
		--when hover over show comparison with other charaters in the raid
		
		local playerSpellsBreakdownCreate = function (tab, frame)
			
			--create the header
			--spell scroll options
			local scroll_width = 860
			local scroll_height = 317
			local scroll_lines = 15
			local scroll_line_height = 20
			local backdrop_color = {.2, .2, .2, 0.2}
			local backdrop_color_on_enter = {.8, .8, .8, 0.4}
			local y = 0
			local headerY = y - 0
			local scrollY = headerY - 20
			local line_colors = {{1, 1, 1, .1}, {1, 1, 1, 0}}
			
			local headerSizeSmall = 50
			local headerSizeMedium = 65
			local headerSizeBig = 90
			
			local petColor = "|cFFCCBBBB"
			local spellNameColor = "|cFFFFFFFF"
			local spellNameSize = 170
			local defaultTextColor = {.89, .89, .89, .89}
			local overHoverTextColor = {.99, .99, .99, .99}
			
			local lineSeparatorWidth = 2
			local lineSeparatorHeight = 336
			
			--target scroll options
			local scroll_height_target = 100
			local scroll_lines_target = 5
			local scroll_lines_height_target = 19
			
			--header
			
			local columnAlign = "right"
			local columnAlignOffset = -5
			
			local headerTable = {
				{text = "Damage Done", width = 300, align = columnAlign, offset = columnAlignOffset, dataType = "number", canSort = true, order = "DESC", selected = true},
				{text = "Casts", width = headerSizeSmall, align = columnAlign, offset = columnAlignOffset, dataType = "number", canSort = true, order = "DESC"},
				{text = "Avg Cast", width = headerSizeMedium, align = columnAlign, offset = columnAlignOffset, dataType = "number", canSort = true, order = "DESC"},
				{text = "Hits", width = headerSizeSmall, align = columnAlign, offset = columnAlignOffset, dataType = "number", canSort = true, order = "DESC"},
				{text = "Avg Hit", width = headerSizeMedium, align = columnAlign, offset = columnAlignOffset, dataType = "number", canSort = true, order = "DESC"},
				{text = "Crit %", width = headerSizeSmall, align = columnAlign, offset = columnAlignOffset, dataType = "number", canSort = true, order = "DESC"},
				{text = "Uptime %", width = headerSizeMedium, align = columnAlign, offset = columnAlignOffset, dataType = "number", canSort = true, order = "DESC"},
				{text = "Miss %", width = headerSizeMedium, align = columnAlign, offset = columnAlignOffset, dataType = "number", canSort = true, order = "DESC"},
				{text = "Dps", width = headerSizeBig, align = columnAlign, offset = columnAlignOffset, dataType = "number", canSort = true, order = "DESC"},
			}

			local headerOnClickCallback = function (headerFrame, columnHeader)
				--set the last actor shown
				tab.last_actor = Details:GetPlayerObjectFromBreakdownWindow()
				--calls the tab fill function to refresh the tab content
				tab:fillfunction (Details:GetPlayerObjectFromBreakdownWindow(), Details:GetCombatFromBreakdownWindow())
			end
			
			local headerOptions = {
				padding = 2,
				header_backdrop_color = {.3, .3, .3, .8},
				header_backdrop_color_selected = {.5, .5, .5, 0.8},
				use_line_separators = true,
				line_separator_color = {.1, .1, .1, .6},
				line_separator_width = lineSeparatorWidth,
				line_separator_height = lineSeparatorHeight,
				line_separator_gap_align = true,
				header_click_callback = headerOnClickCallback,
			}
			
			local header = DF:CreateHeader (frame, headerTable, headerOptions, "DetailsPlayerBreakdownHeader")
			header:SetPoint ("topleft", frame, "topleft", 0, headerY)
			
			local currentSelectedColumn = 1
			
			local line_onenter = function (self)
				self:SetBackdropColor (unpack (backdrop_color_on_enter))
				
				self.spellName.fontcolor = overHoverTextColor
				self.amountDone.fontcolor = overHoverTextColor
				self.percentDone.fontcolor = overHoverTextColor
				self.castAmount.fontcolor = overHoverTextColor
				self.avgCast.fontcolor = overHoverTextColor
				self.hitAmount.fontcolor = overHoverTextColor
				self.avgHits.fontcolor = overHoverTextColor
				self.criticalPercent.fontcolor = overHoverTextColor
				self.auraUptime.fontcolor = overHoverTextColor
				self.missAmount.fontcolor = overHoverTextColor
				self.dpsAmount.fontcolor = overHoverTextColor

				self.spellIcon:SetSize (self.spellIcon.hoverWidth, self.spellIcon.hoverWidth)
			end
			
			local line_onleave = function (self)
				self:SetBackdropColor (unpack (self.BackgroundColor))
				
				self.spellName.fontcolor = defaultTextColor
				self.amountDone.fontcolor = defaultTextColor
				self.percentDone.fontcolor = defaultTextColor
				self.castAmount.fontcolor = defaultTextColor
				self.avgCast.fontcolor = defaultTextColor
				self.hitAmount.fontcolor = defaultTextColor
				self.avgHits.fontcolor = defaultTextColor
				self.criticalPercent.fontcolor = defaultTextColor
				self.auraUptime.fontcolor = defaultTextColor
				self.missAmount.fontcolor = defaultTextColor
				self.dpsAmount.fontcolor = defaultTextColor

				self.spellIcon:SetSize (self.spellIcon.originalWidth, self.spellIcon.originalHeight)
			end
			
			local bgColor, borderColor = {0, 0, 0, 0.8}, {0, 0, 0, 0}
			local line_target_onenter = function (self)
				self:SetBackdropColor (unpack (backdrop_color_on_enter))
				
				local playerObject = self.playerObject
				local attribute = self.attribute
				local combatObject = self.combatObject
				local combatTime = self.combatTime
				local targetName = self.targetName
				local amountDone = self.totalDone
				
				local gameCooltip = GameCooltip2
				
				gameCooltip:Preset (2)
				gameCooltip:SetType ("tooltip")
				gameCooltip:SetOption ("MinWidth", 230)
				gameCooltip:SetOption ("StatusBarTexture", [[Interface\AddOns\Details\images\bar_background]])
				gameCooltip:SetOption ("TextSize", 10)
				gameCooltip:SetOption ("TextFont",  _detalhes.tooltip.fontface)
				gameCooltip:SetOption ("TextColor", _detalhes.tooltip.fontcolor)
				gameCooltip:SetOption ("TextColorRight", _detalhes.tooltip.fontcolor_right)
				gameCooltip:SetOption ("TextShadow", _detalhes.tooltip.fontshadow and "OUTLINE")
				gameCooltip:SetOption ("LeftBorderSize", -4)
				gameCooltip:SetOption ("RightBorderSize", 4)
				gameCooltip:SetOption ("RightTextMargin", 0)
				gameCooltip:SetOption ("VerticalOffset", 8) 
				gameCooltip:SetOption ("AlignAsBlizzTooltip", true)
				gameCooltip:SetOption ("AlignAsBlizzTooltipFrameHeightOffset", -8)
				gameCooltip:SetOption ("LineHeightSizeOffset", 4)
				gameCooltip:SetOption ("VerticalPadding", -4)
				gameCooltip:SetBackdrop (1, Details.cooltip_preset2_backdrop, bgColor, borderColor)
				
				gameCooltip:AddLine ("Spells Used:")
				gameCooltip:AddIcon ("", 1, 1, 16, 16, .1, .9, .1, .9)
				Details:AddTooltipHeaderStatusbar()
				
				local spellsUsedOnTarget = {}
				
				--gather damage on target information from player
				for spellId, spellTable in pairs (playerObject:GetActorSpells()) do
					local damageOnTarget = spellTable.targets [targetName]
					if (damageOnTarget) then
						tinsert (spellsUsedOnTarget, {spellTable, damageOnTarget, false})
					end
				end
				--and from pets the player used
				for _, petName in ipairs (playerObject:Pets()) do
					local petObject = combatObject:GetActor (1, petName)
					for spellId, spellTable in pairs (petObject:GetActorSpells()) do
						local damageOnTarget = spellTable.targets [targetName]
						if (damageOnTarget) then
							tinsert (spellsUsedOnTarget, {spellTable, damageOnTarget, petObject:Name()})
						end
					end
				end
				
				table.sort (spellsUsedOnTarget, function(t1, t2) return t1[2] > t2[2] end)
				local topValue = spellsUsedOnTarget [1] and spellsUsedOnTarget [1] [2]
				local totalValue = playerObject.targets [targetName] or 0
				local r, g, b, a = 0.20, 0.20, 0.20, 0.80
				local truncateString = frame:CreateFontString (nil, "overlay", "GameFontNormal")
				local truncateWidth = 130
				
				for i = 1, #spellsUsedOnTarget do
					local damageInfo = spellsUsedOnTarget [i]
					
					local spellTable = damageInfo [1]
					local totalDamage = damageInfo [2]
					local petName = damageInfo [3]
					
					local formatFunc = Details:GetCurrentToKFunction()
					
					if (totalDamage > 0) then
						local spellName, _, spellIcon = Details.GetSpellInfo (spellTable.id)
						
						if (spellName) then
							local percent = totalDamage / topValue * 100
							
							if (petName) then
								spellName = spellName .. " (|cFFCCBBBB" .. petName:gsub ((" <.*"), "") .. "|r)"
							end
							
							truncateString:SetText (spellName)
							DetailsFramework:TruncateText (truncateString, truncateWidth)
							spellName = truncateString:GetText()
							
							gameCooltip:AddLine (spellName, formatFunc (_, totalDamage) .. " (" .. format ("%.1f", totalDamage / totalValue * 100) .. "%)")
							gameCooltip:AddIcon (spellIcon, 1, 1, 20, 20, .1, .9, .1, .9)
							
							gameCooltip:AddStatusBar (percent, 1, r, g, b, a, false, {value = 100, color = {.21, .21, .21, 0.8}, texture = [[Interface\AddOns\Details\images\bar_serenity]]})
						end
					end
				end
				
				gameCooltip:SetOwner (self, "bottomleft", "topleft", 50, 0)
				gameCooltip:Show()
			end
			
			local line_target_onleave = function (self)
				self:SetBackdropColor (unpack (self.BackgroundColor))
				GameCooltip2:Hide()
			end
			
			--line creation for the target scroll
			local scroll_createline_target = function (self, index)
				local line = CreateFrame ("button", "$parentLine" .. index, self, "BackdropTemplate")
				line:SetPoint ("topleft", self, "topleft", 1, -((index-1) * (scroll_lines_height_target+1)) - 1)
				line:SetSize (scroll_width, scroll_lines_height_target)
				line.id = index
				
				line:SetScript ("OnEnter", line_target_onenter)
				line:SetScript ("OnLeave", line_target_onleave)
				
				line:SetBackdrop ({bgFile = [[Interface\Tooltips\UI-Tooltip-Background]], tileSize = 64, tile = true})
				line:SetBackdropColor (unpack (backdrop_color))
				
				if (index % 2 == 0) then
					line:SetBackdropColor (unpack (line_colors [1]))
					line.BackgroundColor = line_colors [1]
				else
					line:SetBackdropColor (unpack (line_colors [2]))
					line.BackgroundColor = line_colors [2]
				end
				
				local spellNameFrame = CreateFrame ("frame", nil, line, "BackdropTemplate")
				spellNameFrame:SetSize (headerTable [1].width + columnAlignOffset, scroll_lines_height_target)
				spellNameFrame:EnableMouse (false)
				spellNameFrame:SetPoint ("left", line, "left", 0, 0)
				
				--target base info
					--spell icon
					local spellIcon = DF:CreateImage (spellNameFrame, "", spellNameFrame:GetHeight() - 2, spellNameFrame:GetHeight() - 2)
					spellIcon.texcoord = {.1, .9, .1, .9}
					spellIcon:SetPoint ("left", 0, 0)
					
					--spell Name
					local spellName = DF:CreateLabel (spellNameFrame)
					spellName:SetPoint ("left", spellIcon, "right", 2, 0)
					
					--total
					local amountDone = DF:CreateLabel (spellNameFrame)
					amountDone:SetPoint ("right", spellNameFrame, "right", 0, 0)
					
					--percent
					local percentDone = DF:CreateLabel (spellNameFrame)
					percentDone:SetPoint ("right", spellNameFrame, "right", -60, 0)
				
				line.spellIcon = spellIcon
				line.spellName = spellName
				line.amountDone = amountDone
				line.percentDone = percentDone
				
				line.spellName.fontcolor = defaultTextColor
				line.amountDone.fontcolor = defaultTextColor
				line.percentDone.fontcolor = defaultTextColor

				line.spellSlots = {}
				function line:ResetSlots()
					line.nextSpellSlot = 1
				end
				function line:GetSpellSlot (index)
					local slot = 1
				end
				
				return line
			end
			
			local onEnterIcon = function (self)
				local line = self:GetParent():GetParent()
				local spellId = line.spellId
				if (spellId) then
					GameTooltip:SetOwner (self, "ANCHOR_TOPLEFT")
					Details:GameTooltipSetSpellByID (spellId)
					GameTooltip:Show()
				end
				line.spellIcon:SetSize (line.spellIcon.hoverWidth, line.spellIcon.hoverHeight)
			end
			
			local onLeaveIcon = function (self)
				local line = self:GetParent():GetParent()
				GameTooltip:Hide()
				line.spellIcon:SetSize (line.spellIcon.originalWidth, line.spellIcon.originalHeight)
			end
			
			--create line for the spell scroll
			local scroll_createline = function (self, index)
			
				local line = CreateFrame ("button", "$parentLine" .. index, self, "BackdropTemplate")
				line:SetPoint ("topleft", self, "topleft", 1, -((index-1) * (scroll_line_height+1)) - 1)
				line:SetSize (scroll_width, scroll_line_height)
				line.id = index
				
				line:SetScript ("OnEnter", line_onenter)
				line:SetScript ("OnLeave", line_onleave)
				
				line:SetBackdrop ({bgFile = [[Interface\Tooltips\UI-Tooltip-Background]], tileSize = 64, tile = true})
				line:SetBackdropColor (unpack (backdrop_color))
				
				if (index % 2 == 0) then
					line:SetBackdropColor (unpack (line_colors [1]))
					line.BackgroundColor = line_colors [1]
				else
					line:SetBackdropColor (unpack (line_colors [2]))
					line.BackgroundColor = line_colors [2]
				end
				
				DF:Mixin (line, DF.HeaderFunctions)
				
				local spellNameFrame = CreateFrame ("frame", nil, line, "BackdropTemplate")
				spellNameFrame:SetSize (headerTable [1].width + columnAlignOffset, header.options.header_height)
				spellNameFrame:EnableMouse (false)
				
				--spell base info
					--spell icon
					local spellIcon = DF:CreateImage (spellNameFrame, "", spellNameFrame:GetHeight() - 2, spellNameFrame:GetHeight() - 2)
					spellIcon.texcoord = {.1, .9, .1, .9}
					spellIcon:SetPoint ("left", 0, 0)
					spellIcon.originalWidth = spellIcon.width
					spellIcon.originalHeight = spellIcon.height
					spellIcon.hoverWidth = spellIcon.width * 1.15
					spellIcon.hoverHeight = spellIcon.height * 1.15
					
					--show the tooltip when hover the icon
					local iconFrame = CreateFrame ("frame", "$parentIconTooltipFrame", spellNameFrame, "BackdropTemplate")
					iconFrame:SetAllPoints (spellIcon.widget)
					iconFrame:SetScript ("onenter", onEnterIcon)
					iconFrame:SetScript ("onleave", onLeaveIcon)
					iconFrame.iconTexture = spellIcon
					line.iconFrame = iconFrame
					
					--spell Name
					local spellName = DF:CreateLabel (spellNameFrame)
					spellName:SetPoint ("left", spellIcon, "right", 2, 0)
					
					--total
					local amountDone = DF:CreateLabel (spellNameFrame)
					amountDone:SetPoint ("right", spellNameFrame, "right", 0, 0)
					
					--percent
					local percentDone = DF:CreateLabel (spellNameFrame)
					percentDone:SetPoint ("right", spellNameFrame, "right", -60, 0)
				
				--casts amount
				local castAmount = DF:CreateLabel (line)
				
				--avg cast
				local avgCast = DF:CreateLabel (line)
				
				--hit amount
				local hitAmount = DF:CreateLabel (line)
				
				--avg hits
				local avgHits = DF:CreateLabel (line)
				
				--crit amt
				local criticalPercent = DF:CreateLabel (line)
				
				--aura uptime
				local auraUptime = DF:CreateLabel (line)
				
				--miss
				local missAmount = DF:CreateLabel (line)
				
				--dps
				local dpsAmount = DF:CreateLabel (line)
				
				--store the labels into the line object
				line.spellIcon = spellIcon
				line.spellName = spellName
				line.amountDone = amountDone
				line.percentDone = percentDone
				line.castAmount = castAmount
				line.avgCast = avgCast
				line.hitAmount = hitAmount
				line.avgHits = avgHits
				line.criticalPercent = criticalPercent
				line.auraUptime = auraUptime
				line.missAmount = missAmount
				line.dpsAmount = dpsAmount
				
				line.spellName.fontcolor = defaultTextColor
				line.amountDone.fontcolor = defaultTextColor
				line.percentDone.fontcolor = defaultTextColor
				line.castAmount.fontcolor = defaultTextColor
				line.avgCast.fontcolor = defaultTextColor
				line.hitAmount.fontcolor = defaultTextColor
				line.avgHits.fontcolor = defaultTextColor
				line.criticalPercent.fontcolor = defaultTextColor
				line.auraUptime.fontcolor = defaultTextColor
				line.missAmount.fontcolor = defaultTextColor
				line.dpsAmount.fontcolor = defaultTextColor
				
				--align with the header
				line:AddFrameToHeaderAlignment (spellNameFrame)
				line:AddFrameToHeaderAlignment (castAmount)
				line:AddFrameToHeaderAlignment (avgCast)
				line:AddFrameToHeaderAlignment (hitAmount)
				line:AddFrameToHeaderAlignment (avgHits)
				line:AddFrameToHeaderAlignment (criticalPercent)
				line:AddFrameToHeaderAlignment (auraUptime)
				line:AddFrameToHeaderAlignment (missAmount)
				line:AddFrameToHeaderAlignment (dpsAmount)
				
				line:AlignWithHeader (header, "left")
				
				local statusBar = CreateFrame ("statusbar", nil, line, "BackdropTemplate")
				statusBar:SetPoint ("topleft", line, "topleft", spellNameFrame:GetHeight() - 1, 0)
				statusBar:SetPoint ("bottomleft", line, "bottomleft", spellNameFrame:GetHeight() - 1, 0)
				statusBar:SetWidth (headerTable [1].width - spellNameFrame:GetHeight())
				statusBar:SetMinMaxValues (0, 1)
				statusBar:SetValue (1)
				statusBar.texture = statusBar:CreateTexture (nil, "artwork")
				statusBar:SetStatusBarTexture (statusBar.texture)
				statusBar.texture:SetTexture ([[Interface\AddOns\Details_AdvancedPlayerBreakdown\bar1]])
				line.statusBar = statusBar				
				
				return line
			end
			
			--refresh spell list
			local refreshSpellsScroll = function (self, data, offset, totalLines)

				local playerObject = data.playerObject
				local attribute = data.attribute
				local combatObject = data.combatObject
				local combatTime = combatObject:GetCombatTime()				
				local playerTotal = data.playerTotal
				local formatFunc = Details:GetCurrentToKFunction()

				--misc actor object
				local miscObject = combatObject:GetActor (4, playerObject:Name())

				--get the value of the top 1 ranking spell
				local topValue

				--is always getting the player damage done total
				local columnIndex, order = DetailsPlayerBreakdownHeader:GetSelectedColumn()
				if (order == "ASC") then
					topValue = data[#data] and data[#data][4]

				else
					topValue = data[1] and data[1][4]
				end

				--[=[
				--damage done
				if (columnIndex == 1) then
					spellData [4] = spellData [2]

				--casts
				elseif (columnIndex == 2) then
					spellData [4] = castAmount

				--avg cast
				elseif (columnIndex == 3) then
					if (castAmount > 0) then
						spellData [4] = spellTable.total / castAmount
					else
						spellData [4] = spellTable.total
					end

				--hits
				elseif (columnIndex == 4) then
					spellData [4] = spellTable.counter

				--avg hits
				elseif (columnIndex == 5) then
					spellData [4] = spellTable.total / spellTable.counter

				--crit
				elseif (columnIndex == 6) then	
					spellData [4] = spellTable.c_amt ~= 0 and (spellTable.c_amt / spellTable.counter * 100) or 0

				--uptime
				elseif (columnIndex == 7) then
					spellData [4] = uptime

				--miss
				elseif (columnIndex == 8) then
					spellData [4] = spellTable.MISS and spellTable.MISS ~= 0 and (spellTable.MISS / spellTable.counter * 100) or 0

				--dps
				elseif (columnIndex == 9) then
					spellData [4] = spellTable.total / combatTime

				end
				--]=]

				for i = 1, totalLines do
					local index = i + offset

					--dataTable is sorted by damage done DESC
					local dataTable = data [index]
					
					if (dataTable) then
						local line = self:GetLine (i)
						local spellTable = dataTable [1]
						line.spellTable = spellTable
						line.index = index

						local sortedValue = dataTable [4]
						
						local spellId = spellTable.id
						local spellName, _, spellIcon = Details.GetSpellInfo (spellId)
						if (spellName) then

							line.spellIcon.texture = spellIcon
							line.spellIcon.alpha = .8
							line.spellId = spellId
							
							spellName = spellNameColor .. index .. ". "  .. spellName .. "|r"
							
							if (dataTable[3]) then
								local petName = dataTable[3]
								petName = Details:RemoveOwnerName (petName)
								spellName = spellName .. " (" .. petColor .. petName .. "|r)"
							end
							
							local castAmount = 0
							if (miscObject) then
								castAmount = miscObject.spell_cast and miscObject.spell_cast [spellTable.id] or 0
							end
							
							--line.statusBar:SetValue (spellTable.total / topValue)
							line.statusBar:SetValue (sortedValue / topValue)
							local r, g, b = Details:GetSpellSchoolColor (spellTable.spellschool)
							line.statusBar:SetStatusBarColor (r, g, b)

							line.spellName.text = spellName
							DetailsFramework:TruncateText (line.spellName, spellNameSize)
							
							line.amountDone.text = format ("%.1f", spellTable.total / playerTotal * 100) .. "%"
							line.percentDone.text = formatFunc (_, spellTable.total)
							line.castAmount.text = castAmount ~= 0 and castAmount or "-"
							line.avgCast.text = castAmount ~= 0 and formatFunc (_, spellTable.total / castAmount) or "-"
							line.hitAmount.text = spellTable.counter
							line.avgHits.text = formatFunc (_, floor (spellTable.total / spellTable.counter))
							line.criticalPercent.text = spellTable.c_amt ~= 0 and format ("%.1f", spellTable.c_amt / spellTable.counter * 100) .. "%" or "-"
							
							local uptime = "-"
							if (miscObject) then
								local spell = miscObject.debuff_uptime_spells and miscObject.debuff_uptime_spells._ActorTable and miscObject.debuff_uptime_spells._ActorTable [spellTable.id]
								if (spell) then
									uptime = spell.uptime / combatTime * 100
									uptime = format ("%.1f", uptime) .. "%"
								end
							end
							line.auraUptime.text = uptime
							
							line.missAmount.text = spellTable.MISS and spellTable.MISS ~= 0 and format ("%.1f", (spellTable.MISS / spellTable.counter * 100)) .. "%" or "-"

							line.dpsAmount.text = formatFunc (_, floor (spellTable.total / combatTime))
						end
						
					end
				end
			end
			
			--create the spell scroll
			local spellsScroll = DF:CreateScrollBox (frame, "$parentSpellsScroll", refreshSpellsScroll, {}, scroll_width, scroll_height, scroll_lines, scroll_line_height)
			DF:ReskinSlider (spellsScroll)
			spellsScroll:SetPoint ("topleft", frame, "topleft", 0, scrollY)
			--create lines for the spell scroll
			for i = 1, scroll_lines do 
				spellsScroll:CreateLine (scroll_createline)
			end
			--store the scroll within the frame tab
			frame.spellsScroll = spellsScroll
			
			
			--------------------------------------------------------------------
			
			--refresh target list
			local refreshTargetsScroll = function (self, data, offset, totalLines)
			
				--get the value of the top 1 ranking spell
				local topValue = data[1] and data[1][1].total
				local playerTotal = data.playerTotal
				local formatFunc = Details:GetCurrentToKFunction()
				
				local playerObject = data.playerObject
				local attribute = data.attribute
				local combatObject = data.combatObject
				local combatTime = combatObject:GetCombatTime()
				
				local iconTexCoord = {0.50390625, 0.62890625, 0, 0.125}
				
				for i = 1, totalLines do
					local index = i + offset
					local dataTable = data [index]

					if (dataTable) then
						local line = self:GetLine (i)
						
						local targetName = dataTable [1]
						local amountDone = dataTable [2]
						
						line.index = index

						line.spellIcon.texture = [[Interface\AddOns\Details\images\classes_plus]]
						line.spellIcon.texcoord = iconTexCoord
						line.spellIcon.alpha = 1

						line.spellName.text = targetName
						DetailsFramework:TruncateText (line.spellName, spellNameSize)
						
						line.amountDone.text = format ("%.1f", amountDone / playerTotal * 100) .. "%"
						line.percentDone.text = formatFunc (_, amountDone)
						
						--store player and target information on the line
						line.playerObject = playerObject
						line.attribute = attribute
						line.combatObject = combatObject
						line.combatTime = combatTime
						line.targetName = targetName
						line.totalDone = amountDone
					end
				end
			end
			
			--create the target scroll
			local targetsScroll = DF:CreateScrollBox (frame, "$parentTargetsScroll", refreshTargetsScroll, {}, scroll_width, scroll_height_target, scroll_lines_target, scroll_lines_height_target)
			DF:ReskinSlider (targetsScroll)
			targetsScroll:SetPoint ("topleft", spellsScroll, "bottomleft", 0, -40)
			--create lines for the target scroll
			for i = 1, scroll_lines_target do 
				targetsScroll:CreateLine (scroll_createline_target)
			end
			--store the scroll within the frame tab
			frame.targetsScroll = targetsScroll
			
			--targets label
			--frame.targetLabel = DF:CreateLabel (frame, "Targets")
			--frame.targetLabel:SetPoint ("bottomleft", targetsScroll, "topleft", 0, 2)
			
		end
		
		local playerSpellsBreakdownFill = function (tab, player, combat)
			local frame = tab.frame
			local attribute = Details:GetDisplayTypeFromBreakdownWindow()
			
			--get the spells from the player and  fill the scroll box
			local allSpells = {
				playerTotal = player.total,
				playerObject = player,
				attribute = attribute,
				combatObject = combat,
			}
			
			for spellId, spellTable in pairs (player:GetActorSpells()) do
				allSpells [#allSpells + 1] = {spellTable, spellTable.total, false}
			end
			
			for _, petName in ipairs (player:Pets()) do
				local petObject = combat (attribute, petName)
				if (petObject) then
					for spellId, spellTable in pairs (petObject:GetActorSpells()) do
						allSpells [#allSpells + 1] = {spellTable, spellTable.total, petName}
					end
				end
			end
			
			--get which column is currently selected and the sort order
			local columnIndex, order = DetailsPlayerBreakdownHeader:GetSelectedColumn()

			--allSpells structure:
			--[1] spellTable with all information about the spell
			--[2] damage done total
			--[3] is a pet
			--[4] sort total

			local scrollData = {}
			for i = 1, #allSpells do

				local spellData = allSpells [i]
				local spellTable = spellData [1]
				local combatTime = combat:GetCombatTime()

				local castAmount = 0
				local miscObject = combat:GetActor (4, player:Name())
				if (miscObject) then
					castAmount = miscObject.spell_cast and miscObject.spell_cast [spellTable.id] or 0
				end

				local uptime = spellTable.total
				if (miscObject) then
					local spell = miscObject.debuff_uptime_spells and miscObject.debuff_uptime_spells._ActorTable and miscObject.debuff_uptime_spells._ActorTable [spellTable.id]
					if (spell) then
						uptime = spell.uptime / combatTime * 100
					end
				end

				--damage done
				if (columnIndex == 1) then
					spellData [4] = spellData [2]

				--casts
				elseif (columnIndex == 2) then
					spellData [4] = castAmount

				--avg cast
				elseif (columnIndex == 3) then
					if (castAmount > 0) then
						spellData [4] = spellTable.total / castAmount
					else
						spellData [4] = spellTable.total
					end

				--hits
				elseif (columnIndex == 4) then
					spellData [4] = spellTable.counter

				--avg hits
				elseif (columnIndex == 5) then
					spellData [4] = spellTable.total / spellTable.counter

				--crit
				elseif (columnIndex == 6) then	
					spellData [4] = spellTable.c_amt ~= 0 and (spellTable.c_amt / spellTable.counter * 100) or 0

				--uptime
				elseif (columnIndex == 7) then
					spellData [4] = uptime

				--miss
				elseif (columnIndex == 8) then
					spellData [4] = spellTable.MISS and spellTable.MISS ~= 0 and (spellTable.MISS / spellTable.counter * 100) or 0

				--dps
				elseif (columnIndex == 9) then
					spellData [4] = spellTable.total / combatTime

				end
			end

			--table.sort (allSpells, DetailsFramework.SortOrder2)
			if (order == "DESC") then
				table.sort (allSpells, function (t1, t2) return t1[4] > t2[4] end)
			else
				table.sort (allSpells, function (t1, t2) return t1[4] < t2[4] end)
			end
			
			frame.spellsScroll:SetData (allSpells)
			frame.spellsScroll:Refresh()
			
			--targets
			local allTargets = {
				playerTotal = player.total,
				playerObject = player,
				attribute = attribute,
				combatObject = combat,
			}
			
			--spell done to target row
			local allPlayerSpellsOnTarget = {
				playerTotal = player.total,
				playerObject = player,
				attribute = attribute,
				combatObject = combat,
				spellTargets = {},
			}
			
			for targetName, amountDone in pairs (player.targets) do
				allTargets [#allTargets + 1] = {targetName, amountDone}
				allPlayerSpellsOnTarget.spellTargets [targetName] = {}
				
				--player spells
				for spellId, spellTable in pairs (player:GetActorSpells()) do
					local damageOnTarget = spellTable.targets [targetName]
					if (damageOnTarget) then
						allPlayerSpellsOnTarget.spellTargets [targetName] [#allPlayerSpellsOnTarget.spellTargets [targetName] + 1] = {spellId, damageOnTarget}
					end
				end
				
				--player pet
				for _, petName in ipairs (player:Pets()) do
					local petObject = combat:GetActor (attribute, petName)
					if (petObject) then
						for spellId, spellTable in pairs (petObject:GetActorSpells()) do
							local damageOnTarget = spellTable.targets [targetName]
							if (damageOnTarget) then
								allPlayerSpellsOnTarget.spellTargets [targetName] [#allPlayerSpellsOnTarget.spellTargets [targetName] + 1] = {spellId, damageOnTarget, petName}
							end
						end
					end
				end
				
				table.sort (allPlayerSpellsOnTarget.spellTargets [targetName], DetailsFramework.SortOrder2)
			end
			
			table.sort (allTargets, DetailsFramework.SortOrder2)
			
			frame.targetsScroll:SetData (allTargets)
			frame.targetsScroll:Refresh()
			
			--update the targets list
			local startX = 200
			local padding = 100
			
			for i = 1, 10 do
				
			end
			
			
			--show the frame
			frame:Show()
		end
		
		local iconTableCompare = {
			texture = [[Interface\AddOns\Details\images\icons]],
			--coords = {363/512, 381/512, 0/512, 17/512},
			coords = {383/512, 403/512, 0/512, 15/512},
			width = 16,
			height = 14,
		}
		
		_detalhes:CreatePlayerDetailsTab ("New Spell Breakdown", "Spell Breakdown", --[1] tab name [2] localized name
			function (tabOBject, playerObject)  --[3] condition
				local attribute = Details:GetDisplayTypeFromBreakdownWindow()
				local combat = Details:GetCombatFromBreakdownWindow()
				
				if (attribute > 2) then
					return false
				end

				return true
			end, 
			
			playerSpellsBreakdownFill, --[4] fill function
			
			nil, --[5] onclick
			
			playerSpellsBreakdownCreate, --[6] oncreate
			iconTableCompare --icon table [7]
		)
		
		
	end
	
	function aBreakdown:OnEvent (_, event, ...)
	
		if (event == "ADDON_LOADED") then
			local AddonName = select (1, ...)
			if (AddonName == "Details_AdvancedPlayerBreakdown") then
				
				--> every plugin must have a OnDetailsEvent function
				function aBreakdown:OnDetailsEvent (event, ...)
					return handle_details_event (event, ...)
				end
				
				--> Install: install -> if successful installed; saveddata -> a table saved inside details db, used to save small amount of data like configs
				local install, saveddata = Details:InstallPlugin ("RAID", "Advanced Player Breakdown", "Interface\\Icons\\Ability_Warrior_BattleShout", aBreakdown, "DETAILS_PLUGIN_ADVANCED_BREAKDOWN_WINDOW", MINIMAL_DETAILS_VERSION_REQUIRED, "Details! Team", ABREAKDOWN_VERSION)
				if (type (install) == "table" and install.error) then
					print (install.error)
				end
				
				--> registering details events we need
				Details:RegisterEvent (aBreakdown, "COMBAT_PLAYER_ENTER") --when details creates a new segment, not necessary the player entering in combat.
				Details:RegisterEvent (aBreakdown, "COMBAT_PLAYER_LEAVE") --when details finishs a segment, not necessary the player leaving the combat.
				Details:RegisterEvent (aBreakdown, "DETAILS_DATA_RESET") --details combat data has been wiped

				aBreakdown.InstallUtilityTab()
				aBreakdown.InstallAdvancedCompareWindow()
				aBreakdown.InstallNewSpellBreakdown()
			end
		end

	end

end


-- doo